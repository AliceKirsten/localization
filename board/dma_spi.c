/*! ----------------------------------------------------------------------------
 * @file	dma_spi.c
 * @brief	realize fast dma spi read/write; in "safe" mode it used extra RAM buffer
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */
#include "deca_device_api.h"

/***************************************************************************//**
 * Local variables, private define
**/
// Two options of DMA_SPI - with tmp_buf buffer and without
// use  SAFE_DMA_AND_BUFF if you unsure that DMA_SPI works properly
#if	(SAFE_DMA_AND_BUFF == 1)
 #define MAX_DMABUF_SIZE	4096
  static uint8 __align4 	rx_buf[MAX_DMABUF_SIZE];
#endif

/*
 * 	do not use library function cause they are slow
 */
#define DMA_STREAM_CONFIG(x)           (DMA_PeripheralInc_Disable | DMA_MemoryInc_Enable | \
                                        DMA_PeripheralDataSize_Byte | DMA_MemoryDataSize_Byte | \
                                        DMA_PeripheralBurst_Single | DMA_MemoryBurst_Single | \
                                        DMA_Mode_Normal | DMA_Priority_High | x->DMA_Channel)
#define DMA_FIFO_CONFIG                (DMA_FIFOMode_Disable | DMA_FIFOThreshold_1QuarterFull)
#define PORT_DMA_START_TX_FAST(x) 	{x->DMA_TX_CH->CR |= DMA_SxCR_EN;}
#define PORT_DMA_STOP_TX_FAST(x) 	{x->DMA_TX_CH->CR &= ~DMA_SxCR_EN;}
#define PORT_DMA_START_RX_FAST(x) 	{x->DMA_RX_CH->CR |= DMA_SxCR_EN;}
#define PORT_DMA_STOP_RX_FAST(x) 	{x->DMA_RX_CH->CR &= ~DMA_SxCR_EN;}
#define DMA_CLEAN_FLAGS(x)              {x->DMAx->LIFCR = x->DMAx->LISR; x->DMAx->HIFCR = x->DMAx->HISR;}
/***************************************************************************//**
 * Exported function prototypes
 */
void dma_init(SPI_t* SPIChan);

int writetospi_dma(SPI_t* SPIChan,
                   uint16 headerLength,
                   const uint8 *headerBuffer,
                   uint32 bodylength,
                   const uint8 *bodyBuffer);

int readfromspi_dma(SPI_t* SPIChan,
                    uint16 headerLength,
                    const uint8 *headerBuffer,
                    uint32 readlength,
                    uint8 *readBuffer);

/***************************************************************************//**
 * @fn		dma_init()
 * @brief
 * 			init of dma module. we will not use IRQ for DMA transaction
 * 			spi_init should be executed first
 *
**/
void dma_init(SPI_t* SPIChan)
{

/* do not use library function cause it slow */
        SPIChan->SPIx->CR2 |= SPI_I2S_DMAReq_Tx;	    //connect Tx DMA_SPI
        SPIChan->SPIx->CR2 &= ~SPI_I2S_DMAReq_Rx;           //Disconnect Rx DMA_SPI by default

/* DMA Channel SPI_TX Configuration */
        SPIChan->DMA_TX_CH->CR = DMA_STREAM_CONFIG(SPIChan) | DMA_DIR_MemoryToPeripheral;

/* DMA Channel SPI_RX Configuration */
	SPIChan->DMA_RX_CH->CR = DMA_STREAM_CONFIG(SPIChan) | DMA_DIR_PeripheralToMemory;

/* DMA source / destination address */        
        SPIChan->DMA_RX_CH->PAR = SPIChan->DMA_TX_CH->PAR = (uint32)&(SPIChan->SPIx->DR);   

/* DMA FIFO Configuration */
        SPIChan->DMA_RX_CH->FCR = SPIChan->DMA_TX_CH->FCR = DMA_FIFO_CONFIG;

/* Clear status flags */
        DMA_CLEAN_FLAGS(SPIChan);
}


/***************************************************************************//**
 * @fn	writetospi()
 * @brief
 * 		  Low level function to write to the SPI
 * 		  Takes two separate byte buffers for write header and write data
 * 		  Source data will be transfered from buf to spi via DMA
 *
 * @return
 * 			DWT_SUCCESS or DWT_ERROR
 *
**/
#pragma optimize=none
int writetospi_dma
(
        SPI_t* SPIChan,
	uint16 headerLength,
	const uint8 *headerBuffer,
	uint32 bodylength,
	const uint8 *bodyBuffer
)
{
    int stat ;
    
    stat = decamutexon() ;

    PORT_SPI_LO_CS_FAST(SPIChan); // give extra time for SPI slave device

    /*header*/
    SPIChan->DMA_TX_CH->M0AR = (uint32)(headerBuffer) ;
    SPIChan->DMA_TX_CH->NDTR = headerLength ;

    PORT_DMA_START_TX_FAST(SPIChan);		 /* Start transfer */
    while(SPIChan->DMA_TX_CH->NDTR != 0) ;       /* pool until last clock from SPI_TX register done */
    PORT_DMA_STOP_TX_FAST(SPIChan);	         /* do not wait SPI finished cause SPI slow. perform switching on next buffer while spi transaction in wire */
      
    DMA_CLEAN_FLAGS(SPIChan)    // clean all flags
  
    /*data*/
    SPIChan->DMA_TX_CH->M0AR = (uint32)(bodyBuffer);
    SPIChan->DMA_TX_CH->NDTR = bodylength ;

    PORT_DMA_START_TX_FAST(SPIChan);                     /* Start transfer */
    while((SPIChan->DMA_TX_CH->NDTR) != 0);              /* pool until last clock from SPI_TX register done */
    while((SPIChan->SPIx->SR & SPI_I2S_FLAG_BSY) !=0 );  /* Wait until last byte of transmission will leave SPI wire */
    PORT_DMA_STOP_TX_FAST(SPIChan);

    /* finish */
    PORT_SPI_HI_CS_FAST(SPIChan);
    
    SPIChan->SPIx->DR;	        // perform a dummy read operation to clear OverRun &  RxNe flags in SPI SR register

    DMA_CLEAN_FLAGS(SPIChan);   // clean all flags 
    
    decamutexoff(stat); //if (SPIChan == DW1000_SPI)

    return DEV_SUCCESS;
}


/***************************************************************************//**
 * @fn	readfromspi()
 * @brief Source data will be transfered from buf to spi via DMA
 * 		  Low level abstract function to write to the SPI
 * 		  Takes two separate byte buffers for write header and write data
 *
 * @return
 * Low level abstract function to read from the SPI
 * Takes two separate byte buffers for write header and read data
 * returns the offset into read buffer where first byte of read data may be found,
 * or returns -1 if there was an error
 *
 * @return
 * 			DWT_SUCCESS or DWT_ERROR
 *
**/
int readfromspi_dma
(
        SPI_t* SPIChan,
	uint16       headerLength,
	const uint8 *headerBuffer,
	uint32       readLength,
	uint8       *readBuffer
)
{
    int stat;
    
    stat = decamutexon() ;

    SPIChan->SPIx->CR2 |= SPI_I2S_DMAReq_Rx;	/* connect SPI_Tx & SPI_Rx to DMA */
    
    // do not use library function cause it slow
    PORT_SPI_LO_CS_FAST(SPIChan);  // give extra time for SPI slave device

    SPIChan->DMA_TX_CH->M0AR =  (uint32)(headerBuffer);   // no matter what we send to spi after header when perform a reading operation
#if	(SAFE_DMA_AND_BUFF == 1)
    /* clean up safe buffer */
    memset(&rx_buf, 0, headerLength + readLength);
    
    /* destination - read buffer */
    SPIChan->DMA_RX_CH->M0AR = (uint32)(&rx_buf);
    SPIChan->DMA_RX_CH->NDTR = SPIChan->DMA_TX_CH->NDTR = headerLength + readLength; //length
    
    /* Start write transfer*/
    PORT_DMA_START_RX_FAST(SPIChan);
    PORT_DMA_START_TX_FAST(SPIChan);
    while(SPIChan->DMA_RX_CH->NDTR != 0);	/* poll until last clock from SPI_RX */
    PORT_DMA_STOP_TX_FAST(SPIChan);
    PORT_DMA_START_RX_FAST(SPIChan);

    /* result of read operation*/
    memcpy(readBuffer, &rx_buf[headerLength], readLength);

#else
    
    SPIChan->DMA_RX_CH->M0AR = (uint32) readBuffer ;	/* read buffer addr */
    SPIChan->DMA_TX_CH->NDTR = headerLength+readLength;
    SPIChan->DMA_RX_CH->NDTR = headerLength;

    /* Start. write command */

    /* DMA_RX Disable Memory increment */
    SPIChan->DMA_RX_CH->CR = (DMA_STREAM_CONFIG ^ DMA_MemoryInc_Disable); 

    /* start Rx */
    PORT_DMA_START_RX_FAST(SPIChan);
    PORT_DMA_START_TX_FAST(SPIChan);
    while(SPIChan->DMA_RX_CH->NDTR != 0);	/* pool until last clock from SPI_RX */
    PORT_DMA_STOP_RX_FAST(SPIChan);
    PORT_DMA_STOP_TX_FAST(SPIChan);

    SPIChan->DMA_RX_CH->NDTR = readLength;	/* read data length */
    SPIChan->DMA_RX_CH->CR |= (DMA_MemoryInc_Enable);	 /* DMA_Rx Enable Memory increment */
/* DMA_Rx should be started after new readlength and changing of Memory increment mode */
    PORT_DMA_START_RX_FAST(SPIChan);
    PORT_DMA_STOP_TX_FAST(SPIChan);
    while(SPIChan->DMA_RX_CH->NDTR != 0);	/* pool until last clock from SPI_RX */
    PORT_DMA_STOP_TX_FAST(SPIChan);
    PORT_DMA_STOP_RX_FAST(SPIChan);

#endif

    PORT_SPI_HI_CS_FAST(SPIChan);

    SPIChan->SPIx->CR2 &= ~(SPI_I2S_DMAReq_Rx) ;	//Disconnect Rx from DMA_SPI !
   
    DMA_CLEAN_FLAGS(SPIChan); // clean all flags
    
    decamutexoff(stat);
    
    return DEV_SUCCESS;
}

/* eof dma_spi */


