/*! ----------------------------------------------------------------------------
 * @file	port.h
 * @brief	HW specific definitions and functions for portability
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */


#ifndef PORT_H_
#define PORT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "compiler.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#define SYSCLOCK    ((uint32_t)80160000) 

/*****************************************************************************************************************//*
 * To enable Direct Memory Access for SPI set this option to (1)
 * This option will increase speed of spi transactions but it will use an extra RAM memory buffer
 */
#define DMA_ENABLE	                (1)
#define	SAFE_DMA_AND_BUFF               (1) 
#define FLASH_ENABLE	                (0)
#if (FLASH_ENABLE == 1)
    #include "flash_api.h"
#endif
#ifdef STM32F410
    #define RNG_LMUL                    (1.0f / 4294967296.0f)
    #define RNG_HMUL                    (1.0f / 18446744073709551616.0f)
   
    inline uint32_t get_lrand(void)      //return 32bit uint32
    {
        uint32_t rng = 0;
        
        while (!(RNG->SR & RNG_FLAG_DRDY)) asm("nop");
        rng = RNG->DR;
        return rng;
    }
    inline uint64_t get_llrand(void)     //return 64bit uint64
    {
        uint64_t res = 0;
        res = get_lrand();    
        res <<= 32;           
        res |= get_lrand();   
        return res;
    }
    inline float get_drand(void)         //return 32bit float
    {
        return (float)(get_lrand() * RNG_LMUL);
    }
    inline double get_ldrand(void)       //return 64bit double
    {
        return (double)(get_llrand() * RNG_HMUL);
    }
    //return random 32bit uint32 value from 0 to range
    inline uint32_t get_lrand_range(uint32_t range)
    {
        return (uint32_t)(get_ldrand() * range);
    }
#endif
#define DEBUG   	                (1)
/*****************************************************************************************************************/

 typedef enum
{
    LED_1,
    LED_2,
    LED_ALL
} led_t;

/* This structure allows to upcount timer ticks for a long time.
   1 unit = 0x03E8 timer1 ticks (1 ms);
*/
typedef struct 
{
    uint32_t units;
    __IO uint32_t* ticks;
} SysTickCounter_t;

/* This structure allows to downcount timer ticks to operate as a watchdog.
   Allows to set delays between transmissions.
*/
typedef struct 
{
    __IO uint32_t* timer_en;
    __IO uint32_t* timer;
    __IO uint32_t* ticks;
} WatchDogCounter_t;

/* System tick 32 bit variable defined by the platform */
extern __IO SysTickCounter_t clock_count;
extern __IO WatchDogCounter_t event_wdg;

typedef struct
{
    SPI_TypeDef*        SPIx;
    GPIO_TypeDef*       SPIx_GPIO;
    GPIO_TypeDef*       SPIx_CS_GPIO;
    uint8_t             SPIx_AF;
    uint16_t            SPIx_CS;
    uint16_t            SPIx_SCK;
    uint16_t            SPIx_MISO;
    uint16_t            SPIx_MOSI;
    uint16_t            SPIx_PRESCALER;
    DMA_TypeDef*        DMAx;
    uint32_t            DMA_Channel;
    DMA_Stream_TypeDef* DMA_TX_CH;
    DMA_Stream_TypeDef* DMA_RX_CH;
} SPI_t;

#if (FLASH_ENABLE == 1)
    extern SPI_t SPIConfig[2];
    #define DW1000_SPI      &(SPIConfig[0])
    #define FLASH_SPI       &(SPIConfig[1])
#else 
    extern SPI_t SPIConfig;
    #define DW1000_SPI      &(SPIConfig)
#endif

#if (DMA_ENABLE == 1)
 extern void dma_init(SPI_t* SPIChan);
 extern int writetospi_dma( SPI_t* SPIChan,
                            uint16 headerLength,
                            const uint8* headerBuffer,
                            uint32 bodylength,
                            const uint8* bodyBuffer);

 extern int readfromspi_dma(SPI_t* SPIChan,
                            uint16 headerLength,
                            const uint8* headerBuffer,
                            uint32 bodylength,
                            uint8* bodyBuffer);
 #define writetospi     writetospi_dma
 #define readfromspi    readfromspi_dma
#else

 extern int writetospi_serial( SPI_t* SPIChan,
                               uint16 headerLength,
                               const uint8* headerBuffer,
                               uint32 bodylength,
                               const uint8* bodyBuffer);

 extern int readfromspi_serial(SPI_t* SPIChan,
                               uint16 headerLength,
                               const uint8* headerBuffer,
                               uint32 readlength,
                               uint8* readBuffer);

 #define writetospi    writetospi_serial
 #define readfromspi   readfromspi_serial
#endif

#define PORT_SPI_LO_CS_FAST(x)	        x->SPIx_CS_GPIO->BSRR = (x->SPIx_CS << 16);
#define PORT_SPI_HI_CS_FAST(x)	        x->SPIx_CS_GPIO->BSRR = x->SPIx_CS;
#define port_SPI_busy_sending(x)        (SPI_I2S_GetFlagStatus((x->SPIx),(SPI_I2S_FLAG_TXE))==(RESET))
#define port_SPI_no_data(x)		(SPI_I2S_GetFlagStatus((x->SPIx),(SPI_I2S_FLAG_RXNE))==(RESET))
#define port_SPI_send_data(x, data)	SPI_I2S_SendData((x->SPIx),(data))
#define port_SPI_receive_data(x)	SPI_I2S_ReceiveData(x->SPIx)
#define port_SPI_disable(x)		SPI_Cmd(x->SPIx, DISABLE)
#define port_SPI_enable(x)              SPI_Cmd(x->SPIx, ENABLE)
#define port_SPI_set_chip_select(x)	GPIO_SetBits(x->SPIx_CS_GPIO, x->SPIx_CS)
#define port_SPI_clear_chip_select(x)	GPIO_ResetBits(x->SPIx_CS_GPIO, x->SPIx_CS)

#define DW1000IRQ                   GPIO_Pin_3
#define DW1000IRQ_GPIO              GPIOA
#define DW1000IRQ_EXTI              EXTI_Line3
#define DW1000IRQ_EXTI_PORT         EXTI_PortSourceGPIOA 
#define DW1000IRQ_EXTI_PIN          GPIO_PinSource3
#define DW1000IRQ_EXTI_IRQn         EXTI3_IRQn
#define DW1000IRQ_EXTI_USEIRQ       ENABLE
#define DW1000IRQ_EXTI_NOIRQ        DISABLE

#define DW1000RST		    GPIO_Pin_1
#define DW1000RST_GPIO	            GPIOB
#define DW1000RST_EXTI              EXTI_Line1
#define DW1000RST_EXTI_PORT         EXTI_PortSourceGPIOB
#define DW1000RST_EXTI_PIN          GPIO_PinSource1
#define DW1000RST_EXTI_IRQn         EXTI1_IRQn

#define DW1000WUP		    GPIO_Pin_2
#define DW1000WUP_GPIO	            GPIOB

#define LED_PORT                    GPIOB
#if defined(STM32F410Cx)
  #define LED1_PIN                    GPIO_Pin_7
  #define LED2_PIN                    GPIO_Pin_6
#endif
#if defined(STM32F411xE)
  #define LED1_PIN                    GPIO_Pin_9
  #define LED2_PIN                    GPIO_Pin_8
  #define LED_OUT1_PIN                GPIO_Pin_6
  #define LED_OUT2_PIN                GPIO_Pin_5
  #define BSTAT_PIN                   GPIO_Pin_3
  #define TEST_PIN                    GPIO_Pin_4 
#endif

unsigned long long portGetTickCnt(void);
char* port_GET_stack_pointer(void);

extern uint8_t button_status;

ITStatus EXTI_GetITEnStatus(uint32_t x);
void reset_DW1000(void);
void setup_DW1000RSTnIRQ(int enable);
#define port_GetEXT_IRQStatus()             EXTI_GetITEnStatus(DW1000IRQ_EXTI_IRQn)
#define port_DisableEXT_IRQ()               NVIC_DisableIRQ(DW1000IRQ_EXTI_IRQn)
#define port_EnableEXT_IRQ()                NVIC_EnableIRQ(DW1000IRQ_EXTI_IRQn)
#define port_CheckEXT_IRQ()                 GPIO_ReadInputDataBit(DW1000IRQ_GPIO, DW1000IRQ)

void __weak process_dwRSTn_irq(void);
void __weak process_deca_irq(void);
void __weak process_button_irq(void);

int is_IRQ_enabled(void);
void SPI_ChangeRate(SPI_t* SPIChan, uint16_t scalingfactor);
void RNG_Configuration(void);

void led_on(led_t led);
void led_off(led_t led);
void led_toggle(led_t led);

int peripherals_init(void);

#ifdef __cplusplus
}
#endif

#endif /* PORT_H_ */
