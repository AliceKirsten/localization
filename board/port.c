/*! ----------------------------------------------------------------------------
 * @file	port.c
 * @brief	HW specific definitions and functions for portability
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */

#include "port.h"

#if (FLASH_ENABLE == 1)
  #include "flash_api.h"
#endif

#define rcc_init(x)					RCC_Configuration(x)
#define interrupt_init(x)			        NVIC_Configuration(x)
#define spi_init(x)					SPI_Configuration(x)
#define gpio_init(x)				        GPIO_Configuration(x)
#define timer_init(x)   			        Timer_Configuration(x)
#if defined(STM32F410Cx)
    #define rng_init(x)                                 RNG_Configuration(x)
#endif

/* SPI initialization struct array */
#if (FLASH_ENABLE == 1)
    SPI_t SPIConfig[2] = 
    {
      {
        SPI1,
        GPIOA,
        GPIOA,
        GPIO_AF_SPI1,
        GPIO_Pin_4,
        GPIO_Pin_5,
        GPIO_Pin_6,
        GPIO_Pin_7,
        SPI_BaudRatePrescaler_32,
        DMA2,
        DMA_Channel_3,
        DMA2_Stream3,
        DMA2_Stream2
      },
      {
        SPI2,
        GPIOB,
        GPIOB,
        GPIO_AF_SPI2,
        GPIO_Pin_12,
        GPIO_Pin_13,
        GPIO_Pin_14,
        GPIO_Pin_15,
        SPI_BaudRatePrescaler_2,
        DMA1,
        DMA_Channel_0,
        DMA1_Stream4,
        DMA1_Stream3
      }
    };
#else
    SPI_t SPIConfig = 
    {
        SPI1,
        GPIOA,
        GPIOA,
        GPIO_AF_SPI1,
        GPIO_Pin_4,
        GPIO_Pin_5,
        GPIO_Pin_6,
        GPIO_Pin_7,
        SPI_BaudRatePrescaler_32,
        DMA2,
        DMA_Channel_3,
        DMA2_Stream3,
        DMA2_Stream2
    };
#endif
    
__IO SysTickCounter_t clock_count = 
{
    0,
    &TIM5->CNT
};

__IO WatchDogCounter_t event_wdg = 
{
    &TIM1->CCER,
    &TIM1->CCR1,
    &TIM1->CNT
};

unsigned long long portGetTickCnt(void)
{
        /* Timer 1 works as System Core Clock =>
          timer counter contains the tick count value */
      return (clock_count.units * TICKS_IN_UNIT) + *clock_count.ticks;
}

int is_IRQ_enabled(void)
{
	return ((NVIC->ISER[((uint32_t)(DW1000IRQ_EXTI_IRQn) >> 5)]
	         & (uint32_t)0x01 << (DW1000IRQ_EXTI_IRQn & (uint8_t)0x1F)) ? 1 : 0) ;
}

int NVIC_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	// Enable GPIO used as DECA IRQ for interrupt
	GPIO_InitStructure.GPIO_Pin = DW1000IRQ;
	GPIO_InitStructure.GPIO_Mode = 	GPIO_Mode_IN;	//IRQ pin should be Pull Down to prevent unnecessary EXT IRQ while DW1000 goes to sleep mode
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(DW1000IRQ_GPIO, &GPIO_InitStructure);

	/* Connect EXTI Line to GPIO Pin */
	SYSCFG_EXTILineConfig(DW1000IRQ_EXTI_PORT, DW1000IRQ_EXTI_PIN);

	/* Configure EXTI line */
	EXTI_InitStructure.EXTI_Line = DW1000IRQ_EXTI;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;	//MPW3 IRQ polarity is high by default
	EXTI_InitStructure.EXTI_LineCmd = DW1000IRQ_EXTI_USEIRQ;
	EXTI_Init(&EXTI_InitStructure);

	/* Set NVIC Grouping to 16 groups of interrupt without sub-grouping */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	/* Enable and set DW1000 EXTI Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DW1000IRQ_EXTI_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 9;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable the TIM5 Update Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 25;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
        
        /* Enable the TIM1 Capture-Compare Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 27;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

#if defined(STM32F410Cx)
        /* Enable the Random Number Generator Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = RNG_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 80;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
        
	return 0;
}

/**
  * @brief  Checks whether the specified EXTI line is enabled or not.
  * @param  EXTI_Line: specifies the EXTI line to check.
  *   This parameter can be:
  *     @arg EXTI_Linex: External interrupt line x where x(0..19)
  * @retval The "enable" state of EXTI_Line (SET or RESET).
  */
ITStatus EXTI_GetITEnStatus(uint32_t EXTI_Line)
{
  ITStatus bitstatus = RESET;
  uint32_t enablestatus = 0;
  /* Check the parameters */
  assert_param(IS_GET_EXTI_LINE(EXTI_Line));

  enablestatus =  EXTI->IMR & EXTI_Line;
  if (enablestatus != (uint32_t)RESET)
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }
  return bitstatus;
}

uint32_t RCC_Configuration(void)
{
	RCC_ClocksTypeDef RCC_ClockFreq;

	RCC_GetClocksFreq(&RCC_ClockFreq);
        
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Enable SPI1 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
        
        /* Enable DMA2 clock */
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	/* Enable GPIOs clocks */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC, ENABLE);
#if (FLASH_ENABLE == 1)
        /* Enable SPI2 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
        /* Enable DMA1 clock */
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
#endif
        /* Enable TIM5 clock */
        RCC_APB2PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
        
        /* Enable TIM1 clock */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

#if defined(STM32F410Cx)
        /* Enable RNG clock */
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_RNG, ENABLE);
#endif
        
	return RCC_ClockFreq.SYSCLK_Frequency;
}

void reset_DW1000(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	// Enable GPIO used for DW1000 reset
	GPIO_InitStructure.GPIO_Pin = DW1000RST;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DW1000RST_GPIO, &GPIO_InitStructure);
	//drive the RSTn pin low
	GPIO_ResetBits(DW1000RST_GPIO, DW1000RST);

        //put the pin back to tri-state ... as input
	GPIO_InitStructure.GPIO_Pin = DW1000RST;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(DW1000RST_GPIO, &GPIO_InitStructure);
        
        //200 us to wake up then waits 5ms for DW1000 XTAL to stabilise
        //18 us is enought
        usleep(30);
}

void SPI_ChangeRate(SPI_t* SPIChan, uint16_t scalingfactor)
{
	uint16_t tmpreg = 0;
        SPIChan->SPIx_PRESCALER = scalingfactor;

	/* Get the SPIx CR1 value */
	tmpreg = SPIChan->SPIx->CR1;

	/*clear the scaling bits*/
	tmpreg &= 0xFFC7;

	/*set the scaling bits*/
	tmpreg |= SPIChan->SPIx_PRESCALER;

	/* Write to SPIx CR1 */
	SPIChan->SPIx->CR1 = tmpreg;
}

int SPI_Configuration(SPI_t* SPIConf)
{
	SPI_InitTypeDef SPI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

        GPIO_PinAFConfig(SPIConf->SPIx_GPIO, SPIConf->SPIx_SCK, SPIConf->SPIx_AF);
        GPIO_PinAFConfig(SPIConf->SPIx_GPIO, SPIConf->SPIx_MISO, SPIConf->SPIx_AF);
        GPIO_PinAFConfig(SPIConf->SPIx_GPIO, SPIConf->SPIx_MOSI, SPIConf->SPIx_AF);
        
	// SPIx SCK, MISO and MOSI pin setup
	GPIO_InitStructure.GPIO_Pin = SPIConf->SPIx_SCK | SPIConf->SPIx_MISO | SPIConf->SPIx_MOSI;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SPIConf->SPIx_GPIO, &GPIO_InitStructure);

	// SPIx CS pin setup
	GPIO_InitStructure.GPIO_Pin = SPIConf->SPIx_CS;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(SPIConf->SPIx_CS_GPIO, &GPIO_InitStructure);
	
 	// SPIx Mode setup
        SPI_InitStructure.SPI_BaudRatePrescaler = SPIConf->SPIx_PRESCALER;
        SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
        SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
        SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPIConf->SPIx, &SPI_InitStructure);

        // Enable SPIx
	SPI_Cmd(SPIConf->SPIx, ENABLE);
        while(!(SPIConf->SPIx->SR & SPI_I2S_FLAG_TXE)) ;
        
	// Set CS high
	PORT_SPI_HI_CS_FAST(SPIConf);
          
    return 0;
}

void Timer_Configuration(void)
{
      TIM_TimeBaseInitTypeDef TIM_InitStructure;
      
      /* TIMER1 operates as the System Watchdog */
      TIM_InitStructure.TIM_Prescaler = (SYSCLOCK / 1000000);
      TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_InitStructure.TIM_Period = 0xFFFFFFFF;
      TIM_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
      TIM_InitStructure.TIM_RepetitionCounter = 0;
      TIM_TimeBaseInit(TIM1, &TIM_InitStructure);
      /* TIMER5 repeats the System Core Clock */
      TIM_InitStructure.TIM_Period = TICKS_IN_UNIT;
      TIM_TimeBaseInit(TIM5, &TIM_InitStructure);
      
      /* Update source is timer owerflow */
      TIM_UpdateRequestConfig(TIM5, TIM_UpdateSource_Regular);
      
      /* Enable TIM5_Update interrupts */
      TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);
      /* Enable TIM1_CC1 interrupts */
      TIM_ITConfig(TIM1, TIM_IT_CC1, ENABLE);
      
      /* Enable only TIM5 */
      TIM_Cmd(TIM5, ENABLE);
}

void GPIO_Configuration(void)
{
      GPIO_InitTypeDef GPIO_InitStructure;
      
      GPIO_InitStructure.GPIO_Pin = DW1000WUP;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_Init(DW1000WUP_GPIO, &GPIO_InitStructure);

      // Enable GPIO used for LEDs
      GPIO_InitStructure.GPIO_Pin = LED1_PIN | LED2_PIN;
      GPIO_Init(LED_PORT, &GPIO_InitStructure);
      // Enable GPIO used as outputs
      //GPIO_InitStructure.GPIO_Pin = OUTM1_PIN | OUTM2_PIN | OUTA1_PIN | OUTA2_PIN;
      //GPIO_Init(OUT_PORT, &GPIO_InitStructure);
}

#if defined(STM32F410Cx)
void RNG_Configuration(void)
{
    //memset(&rng_queue[0], 0, RNG_QUEUE_LEN*sizeof(uint32_t));
    //RNG_ITConfig(ENABLE);
    RNG_Cmd(ENABLE);            //enable RNG
}
#endif

void led_on(led_t led)
{
    switch (led)
	{
	case LED_1:
		GPIO_SetBits(LED_PORT, LED1_PIN);
		break;
	case LED_2:
		GPIO_SetBits(LED_PORT, LED2_PIN);
		break;
	case LED_ALL:
		GPIO_SetBits(LED_PORT, LED1_PIN | LED2_PIN);
		break;
	default:
		// do nothing for undefined led number
		break;
	}
}

void led_off(led_t led)
{
    switch (led)
	{
	case LED_1:
		GPIO_ResetBits(LED_PORT, LED1_PIN);
		break;
	case LED_2:
		GPIO_ResetBits(LED_PORT, LED2_PIN);
		break;
	case LED_ALL:
		GPIO_ResetBits(LED_PORT, LED1_PIN | LED2_PIN);
		break;
	default:
		// do nothing for undefined led number
		break;
	}
}

void led_toggle(led_t led)
{
    switch (led)
	{
	case LED_1:
		GPIO_ToggleBits(LED_PORT, LED1_PIN);
		break;
	case LED_2:
		GPIO_ToggleBits(LED_PORT, LED2_PIN);
		break;
	case LED_ALL:
		GPIO_ToggleBits(LED_PORT, LED1_PIN | LED2_PIN);
		break;
	default:
		// do nothing for undefined led number
		break;
	}
}

int peripherals_init (void)
{
        if (rcc_init() != SYSCLOCK) return DEV_ERROR;
        
        timer_init();
	gpio_init();
	interrupt_init();
        spi_init(DW1000_SPI);
#if (DMA_ENABLE == 1)
	dma_init(DW1000_SPI);	//init DMA for SPI only. Connection of SPI to DMA in read/write functions
#endif
#if (FLASH_ENABLE == 1)
        spi_init(FLASH_SPI);
        dma_init(FLASH_SPI);
        if (sFLASH_ReadID() != sFLASH_FM25V02G_ID) return DEV_ERROR;
#endif    
#if defined(STM32F410Cx)
        rng_init();
#endif
        
	return DEV_SUCCESS;
}