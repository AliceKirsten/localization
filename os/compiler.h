/*! ----------------------------------------------------------------------------
 * @file	compiler.h
 * @brief
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */

#ifndef COMPILER_H_
#define COMPILER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#ifdef __GNUC__
#include <stdbool.h>
#ifndef __cplusplus
#ifndef TRUE
#define TRUE true
#endif
#ifndef FALSE
#define FALSE false
#endif
#endif
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#define __align4            __attribute__((aligned (4)))
#define __weak              __attribute__((weak))
#define __always_inline     __attribute__((always_inline))
#endif

#ifdef __ICCARM__
#include <yfuns.h>
#define __caddr_t_required_
#define __useconds_t_required_
#define __tms_struct_required_
#define __stat_struct_required_
#define STDIN_FILENO        _LLIO_STDIN
#define STDOUT_FILENO       _LLIO_STDOUT
#define STDERR_FILENO       _LLIO_STDERR
#define __align4            /* to be defined? */
#define __weak              /* to be defined? */
#define _exit               __exit
#include "syscalls.h"
#define sleep(x)            usleep(x*1000000)
#define msleep(x)           usleep(x*1000)
#endif

#ifdef _MSC_VER
#include <windows.h>
#include <share.h>
#define __align4            /* no implementation */
#define __weak              /* no implementation */
#define __always_inline
#else
#define sprintf_s(x,y,...)      0   // temporary workaround
#endif

#ifndef __IO
#define __IO                    volatile
#endif

#ifndef uint8
#ifndef _DECA_UINT8_
#define _DECA_UINT8_
typedef unsigned char uint8;
#endif
#endif

#ifndef uint16
#ifndef _DECA_UINT16_
#define _DECA_UINT16_
typedef unsigned short uint16;
#endif
#endif

#ifndef uint32
#ifndef _DECA_UINT32_
#define _DECA_UINT32_
typedef unsigned long uint32;
#endif
#endif

#ifndef int8
#ifndef _DECA_INT8_
#define _DECA_INT8_
typedef signed char int8;
#endif
#endif

#ifndef int16
#ifndef _DECA_INT16_
#define _DECA_INT16_
typedef signed short int16;
#endif
#endif

#ifndef int32
#ifndef _DECA_INT32_
#define _DECA_INT32_
typedef signed long int32;
#endif
#endif

typedef uint64_t        uint64 ;

typedef int64_t         int64 ;

#ifndef bool
#ifndef __cplusplus
typedef enum _bool_t 
{
  false = 0, 
  true
} bool;
#else
#ifndef true
#define true                    1
#endif
#ifndef false
#define false                   0
#endif
#endif
#endif
  
#define DEV_ERROR               -1
#define DEV_SUCCESS              0
  
/* Redefine TICKS_IN_UNIT to make it work with Sleep(1) */
#define TICKS_IN_UNIT           0x03E8//0x05DC - 1,5 ms//0x03E8 - 1 ms

#ifdef __cplusplus
}
#endif

#endif /* COMPILER_H_ */
