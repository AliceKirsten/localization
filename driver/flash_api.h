/*! ----------------------------------------------------------------------------
 *  @file    flash_api.h
 *  @brief   Flash memory FM25V02 Register Definitions
 * 	     This file supports assembler and C development for FM25V02 enabled devices
 *
 * @attention
 *
 * Copyright 2016 (c) HTSYS, Moscow, Russia.
 *
 * All rights reserved.
 *
 * @author HTSYS
 */

#ifndef _FLASH_API_H_
#define _FLASH_API_H_

#ifdef __cplusplus
extern "C" {
#endif
  
#define FLASH_API_ERROR_CHECK     (0)
  
/* M25P SPI Flash supported commands */  
#define sFLASH_CMD_WRSR           0x01  /* Write Status Register instruction */
#define sFLASH_CMD_WRITE          0x02  /* Write to Memory instruction */
#define sFLASH_CMD_READ           0x03  /* Read from Memory instruction */
#define sFLASH_CMD_WRDI           0x04  /* Reset write enable latch */
#define sFLASH_CMD_RDSR           0x05  /* Read Status Register instruction  */
#define sFLASH_CMD_WREN           0x06  /* Write enable instruction */
#define sFLASH_CMD_FSTRD          0x0B  /* Fast read memory */
#define sFLASH_CMD_RDID           0x9F  /* Read identification */
#define sFLASH_CMD_SLEEP          0xB9  /* Enter sleep mode */

#define IS_sFLASH_CMD(CMD)      (((CMD) == sFLASH_CMD_WRSR)  || \
                                 ((CMD) == sFLASH_CMD_WRITE) || \
                                 ((CMD) == sFLASH_CMD_READ)  || \
                                 ((CMD) == sFLASH_CMD_WRDI)  || \
                                 ((CMD) == sFLASH_CMD_RDSR)  || \
                                 ((CMD) == sFLASH_CMD_WREN)  || \
                                 ((CMD) == sFLASH_CMD_FSTRD) || \
                                 ((CMD) == sFLASH_CMD_RDID)  || \
                                 ((CMD) == sFLASH_CMD_SLEEP))

#define sFLASH_START_ADDR         0x0000
#define sFLASH_END_ADDR           0x7FFF
#define sFLASH_SIZE               0x7D00
#define sFLASH_DUMMY_ADDR         0xFFFF  
#define sFLASH_DUMMY_BYTE         0x5A
#define sFLASH_FM25V02G_ID        0x7F7F7F7F7FC22200
  
/**
  * @brief  Constructs a header in case of command and address params and write a message in the flash mem.
  * @param  command: command to flash device;
            address: address to write;
            length: length of the message in bytes;
            buffer: pointer to the message.
  * @retval DEV_SUCCESS or DEV_ERROR.
  */
int sFLASH_writetodevice
(   uint16      command,
    uint16      address,
    uint16      length,
    uint8       *buffer
);

/**
  * @brief  Constructs a header in case of command and address params and read a mesage from the flash mem.
  * @param  command: command to flash device;
            address: address to read;
            length: length of the message in bytes;
            buffer: pointer to the buffer for the message.
  * @retval DEV_SUCCESS or DEV_ERROR.
  */
int sFLASH_readfromdevice
(   uint16      command,
    uint16      address,
    uint16      length,
    uint8       *buffer
);

/**
  * @brief  Set the write access status to the FRAM.
  * @param  state: enabe\disable status
  * @retval None
  */
void sFLASH_SetWriteAccess(FunctionalState state);

/**
  * @brief  Erases the specified FLASH sector.
  * @param  SectorAddr: address of the sector to erase;
            NumBytes: count of bytes to erase.
  * @retval none.
  */
void sFLASH_EraseSector(uint16 StartAddr, uint16 NumBytes);

/**
  * @brief  Erases the entire FLASH.
  * @param  None
  * @retval None
  */
void sFLASH_EraseBulk(void);

/**
  * @brief  Reads FRAM identification.
  * @param  None
  * @retval FRAM identification
  */
uint64_t sFLASH_ReadID(void);

/**
  * @brief  Writes more than one byte to the FRAM with a single WRITE cycle 
  *         (Page WRITE sequence).
  * @param  pBuffer: pointer to the buffer  containing the data to be written
  *         to the FRAM.
  * @param  Addr: FRAM's internal address to write to.
  * @param  NumBytes: number of bytes to write to the FRAM
  * @retval None
  */
void sFLASH_WriteBuffer(uint8* pBuffer, uint16 Addr, uint16 NumBytes);

/**
  * @brief  Reads a block of data from the FRAM.
  * @param  pBuffer: pointer to the buffer that receives the data read from the FRAM.
  * @param  Addr: FRAM's internal address to read from.
  * @param  NumBytes: number of bytes to read from the FRAM.
  * @retval None
  */
void sFLASH_ReadBuffer(uint8* pBuffer, uint16 Addr, uint16 NumBytes);

#ifdef __cplusplus
}
#endif

#endif /* _FLASH_API_H */