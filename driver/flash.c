
/* Includes ------------------------------------------------------------------*/
#include "port.h"
#if (FLASH_ENABLE == 1)
#include "flash_api.h"

/** @addtogroup STM32F4xx_StdPeriph_Examples
  * @{
  */

/* "writetospi" and "readfromspi" function prototypes are declared in "port.h" header file. 
    It's input parameters:
        SPI_t* SPIChan - pointer to SPI configuration structure, defined in "port.h",
                         use FLASH_SPI preprocessor directive to communicate with the flash memory unit;
        uint16_t headerLength - command buffer's length;
        const uint8_t* headerBuffer - command;
        uint32_t bodylength - message buffer's length;
        const uint8_t* bodyBuffer - message.
*/
/**
  * @brief  Constructs a header in case of command and address params and write a message in the flash mem.
  * @param  command: command to flash device;
            address: address to write;
            length: length of the message in bytes;
            buffer: pointer to the message.
  * @retval DEV_SUCCESS or DEV_ERROR.
  */
int sFLASH_writetodevice
(   uint16      command,
    uint16      address,
    uint16      length,
    uint8 *buffer
)
{
    uint8 header[3];                                       // buffer to compose header in
    int   cnt = 0;                                         // counter for length of header
#if (FLASH_API_ERROR_CHECK == 1)
    if (!IS_sFLASH_CMD(command)) return DEV_ERROR;         // command list is defined at header file
#endif
    header[cnt++] = command;                               // white command to header
    
    if (address != sFLASH_DUMMY_ADDR)
    {
#if (FLASH_API_ERROR_CHECK == 1)
        if (address > sFLASH_END_ADDR) return DEV_ERROR;   // address is limited to 15-bits.
        if (length > sFLASH_SIZE) return DEV_ERROR;        // flash size is 256 kb
#endif
        header[cnt++] = (address & 0xFF00) >> 8;           // write msb of address
        header[cnt++] = address & 0xFF;                    // write lsb of address
    }

    return writetospi(FLASH_SPI, cnt, header, length, buffer);
}


/**
  * @brief  Constructs a header in case of command and address params and read a mesage from the flash mem.
  * @param  command: command to flash device;
            address: address to read;
            length: length of the message in bytes;
            buffer: pointer to the buffer for the message.
  * @retval DEV_SUCCESS or DEV_ERROR.
  */
int sFLASH_readfromdevice
(   uint16      command,
    uint16      address,
    uint16      length,
    uint8       *buffer
)
{
    uint8 header[3];                                       // buffer to compose header in
    int   cnt = 0;                                         // counter for length of header
#if (FLASH_API_ERROR_CHECK == 1)
    if (!IS_sFLASH_CMD(command)) return DEV_ERROR;         // command list is defined at header file
#endif
    header[cnt++] = command;                               // white command to header
    
    if (address != sFLASH_DUMMY_ADDR)
    {
#if (FLASH_API_ERROR_CHECK == 1)
        if (address > sFLASH_END_ADDR) return DEV_ERROR;   // address is limited to 15-bits.
        if (length > sFLASH_SIZE) return DEV_ERROR;        // flash size is 256 kb
#endif
        header[cnt++] = (address & 0xFF00) >> 8;           // write msb of address
        header[cnt++] = address & 0xFF;                    // write lsb of address
    }

    return readfromspi(FLASH_SPI, cnt, header, length, buffer);
}

/**
  * @brief  Set the write access status to the FRAM.
  * @param  state: enabe\disable status
  * @retval None
  * Modified for FM25V02-G
  */
void sFLASH_SetWriteAccess(FunctionalState state)
{
   uint8 byte = 0;
   
   if (state) sFLASH_writetodevice(sFLASH_CMD_WREN, sFLASH_DUMMY_ADDR, 0, &byte);
   else sFLASH_writetodevice(sFLASH_CMD_WRDI, sFLASH_DUMMY_ADDR, 0, &byte);
}

/**
  * @brief  Erases the specified FLASH sector.
  * @param  SectorAddr: address of the sector to erase;
            NumBytes: count of bytes to erase.
  * @retval none.
  */
void sFLASH_EraseSector(uint16 StartAddr, uint16 NumBytes)
{
  //make dump buffer
  uint8* buffer = (uint8*)malloc(NumBytes);
  for (int i = 0; i < NumBytes; i++) buffer[i] = 0xFF;
  
  /*!< Enable the write access to the FLASH */
  sFLASH_SetWriteAccess(ENABLE);

  /*!< Send "Write to Memory " instruction */
  sFLASH_writetodevice(sFLASH_CMD_WRITE, StartAddr, NumBytes, buffer);
}

/**
  * @brief  Erases the entire FLASH.
  * @param  None
  * @retval None
  */
void sFLASH_EraseBulk(void)
{
  sFLASH_EraseSector(sFLASH_START_ADDR, sFLASH_SIZE);
}

/**
  * @brief  Reads FRAM identification.
  * @param  None
  * @retval FRAM identification
  */
uint64_t sFLASH_ReadID(void)
{
  
  uint8 buffer[9];
  uint64 id = 0;
  /*!< Send "RDID" instruction */
  sFLASH_readfromdevice(sFLASH_CMD_RDID, sFLASH_DUMMY_ADDR, 9, &buffer[0]);
  
  //Cypress FM25V02-G Device ID has 9 bytes, but for 64-bit compatibility we use only 8 bytes.
  id = (buffer[1] << 24) | (buffer[2] << 16) | (buffer[3] << 8) | buffer[4];
  id <<= 32;
  id |= (buffer[5] << 24) | (buffer[6] << 16) | (buffer[7] << 8) | buffer[8];
  
  return id;
}

/**
  * @brief  Writes more than one byte to the FRAM with a single WRITE cycle 
  *         (Page WRITE sequence).
  * @param  pBuffer: pointer to the buffer  containing the data to be written
  *         to the FRAM.
  * @param  Addr: FRAM's internal address to write to.
  * @param  NumBytes: number of bytes to write to the FRAM
  * @retval None
  */
void sFLASH_WriteBuffer(uint8* pBuffer, uint16 Addr, uint16 NumBytes)
{
  /*!< Enable the write access to the FLASH */
  sFLASH_SetWriteAccess(ENABLE);

  //write sequence
  sFLASH_writetodevice(sFLASH_CMD_WRITE, Addr, NumBytes, pBuffer);
}

/**
  * @brief  Reads a block of data from the FRAM.
  * @param  pBuffer: pointer to the buffer that receives the data read from the FRAM.
  * @param  Addr: FRAM's internal address to read from.
  * @param  NumBytes: number of bytes to read from the FRAM.
  * @retval None
  */
void sFLASH_ReadBuffer(uint8* pBuffer, uint16 Addr, uint16 NumBytes)
{
  //read sequence
  sFLASH_readfromdevice(sFLASH_CMD_READ, Addr, NumBytes, pBuffer);
}

#endif  //FLASH_ENABLE == 1
/**
  * @}
  */

/**
  * @}
  */
