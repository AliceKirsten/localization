/*! ----------------------------------------------------------------------------
 *  @file    instance_common.c
 *  @brief   DecaWave application level common instance functions
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */
#include "compiler.h"
#include "port.h"
#include "deca_device_api.h"

#include "instance.h"


// -------------------------------------------------------------------------------------------------------------------
//      Data Definitions
// -------------------------------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------------------------

instance_data_t instance_data;
// -------------------------------------------------------------------------------------------------------------------
// Functions
// -------------------------------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------------------------
// convert microseconds to device time
uint64 convertmicrosectodevicetimeu (double microsecu)
{
    uint64 dt;
    long double dtime;

    dtime = (microsecu / (double) DWT_TIME_UNITS) / 1e6 ;

    dt =  (uint64) (dtime) ;

    return dt;
}

// -------------------------------------------------------------------------------------------------------------------
// convert microseconds to device time
uint32 convertmicrosectodevicetimeu32 (double microsecu)
{
    uint32 dt;
    long double dtime;

    dtime = (microsecu / (double) DWT_TIME_UNITS) / 1e6 ;

    dt =  (uint32) (dtime) ;

    return dt;
}


double convertdevicetimetosec(int32 dt)
{
    double f = 0;

    f =  dt * DWT_TIME_UNITS ;  // seconds #define TIME_UNITS          (1.0/499.2e6/128.0) = 15.65e-12

    return f ;
}

double convertdevicetimetosec8(uint8* dt)
{
    double f = 0;

    uint32 lo = 0;
    int8 hi = 0;

    memcpy(&lo, dt, 4);
    hi = dt[4] ;

    f = ((hi * 0x100000000) + lo) * DWT_TIME_UNITS ;  // seconds #define TIME_UNITS          (1.0/499.2e6/128.0) = 15.65e-12

    return f ;
}

// -------------------------------------------------------------------------------------------------------------------
#if NUM_INST != 1
#error These functions assume one instance only
#else
// -------------------------------------------------------------------------------------------------------------------
// Set this instance role as the Tag, Anchor or Listener
void instancesetrole(int inst_mode)
{
    instance_data.mode =  inst_mode;                   // set the role
}

int instancegetrole(void)
{
    return instance_data.mode;
}

int instance_isconnect(void)
{
    return instance_data.connect;
}

// -------------------------------------------------------------------------------------------------------------------
// function to clear counts/averages/range values
//
void instanceclearcounts(void)
{
    dwt_configeventcounters(ENABLE); //enable and clear - NOTE: the counters are not preserved when in DEEP SLEEP

    instance_data.rxTimeouts = 0 ;
    instance_data.frame_sn = 0;
    instance_data.Anchor_list_len = 0;
    instance_data.Anchor_list_index = 0;

} // end instanceclearcounts()

// -------------------------------------------------------------------------------------------------------------------
//
// function to select the destination address (e.g. the address of the next anchor to poll)
//
// -------------------------------------------------------------------------------------------------------------------
//
int instaddanchortolist(uint8* ancAddr)
{
    uint8 i;
    uint8 blank[8] = {0, 0, 0, 0, 0, 0, 0, 0};

    //add the new Anchor to the list, if not already there and there is space
    for(i = 0; i < ANCHOR_LIST_SIZE; i++)
    {
        if(memcmp(&instance_data.Anchor_address_list[i][0], &ancAddr[0], 8) != 0) //if it is not the same address
        {
            if(memcmp(&instance_data.Anchor_address_list[i][0], &blank[0], 8) == 0) //if it is a free space
            {
                memcpy(&instance_data.Anchor_address_list[0], &ancAddr[0], 8) ;
                instance_data.Anchor_list_len = i + 1;
                break;
            }
        }
        else
        {
            break; //we already have this Anchor in the list
        }
    }

    return 0;
}

void instselectanchor(void)
{
    #if (USING_64BIT_ADDR == 1)
        memcpy(&instance_data.poll_msg.destAddr[0], &instance_data.Anchor_address_list[instance_data.Anchor_list_index][0], ADDR_BYTE_SIZE_L); //copy the next long anchor adress
    #else
        memcpy(&instance_data.poll_msg.destAddr[0], &instance_data.Anchor_address_list[instance_data.Anchor_list_index][0], ADDR_BYTE_SIZE_S); //copy the next short anchor adress
    #endif
    
    if (instance_data.Anchor_list_index < instance_data.Anchor_list_len - 1) instance_data.Anchor_list_index++;
    else instance_data.Anchor_list_index = 0;
}

// -------------------------------------------------------------------------------------------------------------------
// function to initialise instance structures
//
// Returns 0 on success and -1 on error
int instance_init(void)
{
    int result;

#if (DEEP_SLEEP_AUTOWAKEUP == 1)
	uint16 blinktime  = 0xf; //e.g. blink time to be used for the Tag auto wake up
    {
        double t;
		uint16 lp_osc_cal = dwt_calibratesleepcnt(); //calibrate low power oscillator
        //the lp_osc_cal value is number of XTAL/2 cycles in one cycle of LP OSC
        //to convert into seconds (38.4MHz/2 = 19.2MHz (XTAL/2) => 1/19.2MHz ns)
        //so to get a sleep time of 5s we need to program 5 / period and then >> 12 as the register holds upper 16-bits of 28-bit counter
		t = ((double)5/((double)lp_osc_cal/19.2e6));
		blinktime = (int) t;
		blinktime >>= 12;

		dwt_configuresleepcnt(blinktime);//configure sleep time
    }
#endif

    //we can enable any configuration loding from OTP/ROM on initialisation
    result = dwt_initialise(DWT_LOADUCODE | DWT_LOADLDO | DWT_LOADTXCONFIG | DWT_LOADANTDLY| DWT_LOADXTALTRIM) ;

    //this is platform dependant - only program if DW EVK/EVB
    dwt_setleds(3) ; //configure the GPIOs which control the leds on EVBs

    if (DWT_SUCCESS != result)
    {
        return (-1) ;   // device initialise has failed
    }

    instance_data.panid = 0xdeca ;
    sprintf(&instance_data.eui64[0], "htsystag");
    instance_data.stoptimer = 0;
    instance_data.instancetimer_en = 0;
    instance_data.rxautoreenable = 1;
    
    instanceclearcounts();
    instance_clearevents();
    return 0 ;
}

// -------------------------------------------------------------------------------------------------------------------
//
// Return the Device ID register value, enables higher level validation of physical device presence
//

uint32 instancereaddeviceid(void)
{
    return dwt_readdevid() ;
}


// -------------------------------------------------------------------------------------------------------------------
//
// function to allow application configuration be passed into instance and affect underlying device opetation
//
void instance_config(instanceConfig_t *config, uint8 manualpoweren)
{
    int use_otpdata = DWT_LOADANTDLY | DWT_LOADXTALTRIM;
    uint32 power = 0;

    instance_data.configData.chan = config->channelNumber ;
    instance_data.configData.rxCode =  config->preambleCode ;
    instance_data.configData.txCode = config->preambleCode ;
    instance_data.configData.prf = config->pulseRepFreq ;
    instance_data.configData.dataRate = config->dataRate ;
    instance_data.configData.txPreambLength = config->preambleLen ;
    instance_data.configData.rxPAC = config->pacSize ;
    instance_data.configData.nsSFD = config->nsSFD ;
    instance_data.configData.sfdTO = config->sfdTO;
    instance_data.configData.phrMode = config->phrMode;
    if(instance_data.configData.dataRate == DWT_BR_6M8 && !manualpoweren) instance_data.configData.smartPowerEn = 1;
    else instance_data.configData.smartPowerEn = 0;
    //configure the channel parameters
    dwt_configure(&instance_data.configData, use_otpdata) ;

    instance_data.configTX.PGdly = txSpectrumConfig[config->channelNumber].PGdelay ;

   //Configure TX power
    if (manualpoweren)
    {
        power = config->powerTX;
    }
    else
    {
        //firstly check if there are calibrated TX power value in the DW1000 OTP
        power = dwt_getotptxpower(config->pulseRepFreq, instance_data.configData.chan);

        if((power == 0x0) || (power == 0xFFFFFFFF)) //if there are no calibrated values... need to use defaults
        {
            power = txSpectrumConfig[config->channelNumber].txPwr[config->pulseRepFreq - DWT_PRF_16M];
        }
    }
    //if smart power is used then the value as read from OTP is used directly
    //if smart power is used the user needs to make sure to transmit only one frame per 1ms or TX spectrum power will be violated
    if(instance_data.configData.smartPowerEn)
    {
        instance_data.configTX.power = power;
    }
    else //if the smart power is not used, then the low byte value (repeated) is used for the whole TX power register
    {
        uint8 pow = power & 0xFF;
        instance_data.configTX.power = (pow | (pow << 8) | (pow << 16) | (pow << 24));
    }
    dwt_setsmarttxpower(instance_data.configData.smartPowerEn);

    //configure the tx spectrum parameters (power and PG delay)
    dwt_configuretxrf(&instance_data.configTX);

    //check if to use the antenna delay calibration values as read from the OTP
    if((use_otpdata & DWT_LOADANTDLY) == 0)
    {
        instance_data.txantennaDelay = rfDelays[config->pulseRepFreq - DWT_PRF_16M];
        // -------------------------------------------------------------------------------------------------------------------
        // set the antenna delay, we assume that the RX is the same as TX.
        dwt_setrxantennadelay(instance_data.txantennaDelay);
        dwt_settxantennadelay(instance_data.txantennaDelay);
    }
    else
    {
        //get the antenna delay that was read from the OTP calibration area
        instance_data.txantennaDelay = dwt_readantennadelay(config->pulseRepFreq) >> 1;

        // if nothing was actually programmed then set a reasonable value anyway
        if (instance_data.txantennaDelay == 0)
        {
            instance_data.txantennaDelay = rfDelays[config->pulseRepFreq - DWT_PRF_16M];
            // -------------------------------------------------------------------------------------------------------------------
            // set the antenna delay, we assume that the RX is the same as TX.
            dwt_setrxantennadelay(instance_data.txantennaDelay);
            dwt_settxantennadelay(instance_data.txantennaDelay);
        }


    }

    if(config->preambleLen == DWT_PLEN_64) //if preamble length is 64
    {
        SPI_ConfigFastRate(SPI_BaudRatePrescaler_32); //reduce SPI to < 3MHz

	dwt_loadopsettabfromotp(0);

	SPI_ConfigFastRate(SPI_BaudRatePrescaler_4); //increase SPI to max
    }
}

int instance_get_dly(void) //get antenna delay
{
    return instance_data.txantennaDelay;
}


// -------------------------------------------------------------------------------------------------------------------
void instance_txcallback(const dwt_callback_data_t *txd)
{
	uint8 txTimeStamp[5] = {0, 0, 0, 0, 0};

	uint8 txevent = txd->event;
	event_data_t dw_event;

	if(txevent == DWT_SIG_TX_DONE)
	{
		dwt_readtxtimestamp(txTimeStamp) ;
		dw_event.timeStamp32l = (uint32)txTimeStamp[0] + ((uint32)txTimeStamp[1] << 8) + ((uint32)txTimeStamp[2] << 16) + ((uint32)txTimeStamp[3] << 24);
		dw_event.timeStamp = txTimeStamp[4];
                dw_event.timeStamp <<= 32;
		dw_event.timeStamp += dw_event.timeStamp32l;
		dw_event.timeStamp32h = ((uint32)txTimeStamp[4] << 24) + (dw_event.timeStamp32l >> 8);
                dw_event.rxLength = 0;
		dw_event.type = DWT_SIG_TX_DONE ;
                
		instance_putevent(dw_event);

	}
	else if(txevent == DWT_SIG_TX_AA_DONE)
	{
		//auto ACK confirmation
		dw_event.rxLength = 0;
		dw_event.type = DWT_SIG_TX_AA_DONE ;

		instance_putevent(dw_event);
	}
}

void instance_rxcallback(const dwt_callback_data_t *rxd)
{
	uint8 rxTimeStamp[5]  = {0, 0, 0, 0, 0};

        uint8 rxd_event = 0;
	event_data_t dw_event;

	//if we got a frame with a good CRC - RX OK
        if(rxd->event == DWT_SIG_RX_OKAY)
	{
                dwt_readrxdata((uint8 *)&dw_event.msgu.frame[0], rxd->datalength, 0);  // Read Data Frame
                //the overrun has occured while we were reading the data - dump the frame/data
                if(dwt_checkoverrun()) 
                {
                    instancerxon(&instance_data, 0, 0); //immediate enable
                    return;
                }
                  
                rxd_event = DWT_SIG_RX_OKAY;
                dw_event.rxLength = rxd->datalength;

		//need to process the frame control bytes to figure out what type of frame we have received
                switch(rxd->fctrl[0])
                {
			//blink type frame
                        case 0xC5:
                        {
                                //blink with Temperature and Battery level indication
				if(rxd->datalength == 12 || rxd->datalength == 18)
				{
					rxd_event = SIG_RX_BLINK;
				}
				else rxd_event = SIG_RX_UNKNOWN;
                        }
                        break;
			//ACK type frame
			case 0x02:
				if(rxd->datalength == 5) rxd_event = SIG_RX_ACK;
                                break;

			//data type frames (with/without ACK request) - assume PIDC is on.
			case 0x41:
			case 0x61:
				//read the frame
				if(memcmp(&instance_data.eui64[0], &dw_event.msgu.rxmsg.destAddr, 8) != 0 || rxd->datalength > STANDARD_FRAME_SIZE) rxd_event = SIG_RX_UNKNOWN;
                                break;
            		//any other frame types are not supported by this application
			default:
				rxd_event = SIG_RX_UNKNOWN;
				break;
		}

                //read rx timestamp
		if((rxd_event == SIG_RX_ACK) || (rxd_event == SIG_RX_BLINK) || (rxd_event == DWT_SIG_RX_OKAY))
		{
                        dw_event.type = rxd_event;
			dwt_readrxtimestamp(rxTimeStamp) ;
			dw_event.timeStamp32l =  (uint32)rxTimeStamp[0] + ((uint32)rxTimeStamp[1] << 8) + ((uint32)rxTimeStamp[2] << 16) + ((uint32)rxTimeStamp[3] << 24);
			dw_event.timeStamp = rxTimeStamp[4];
			dw_event.timeStamp <<= 32;
			dw_event.timeStamp += dw_event.timeStamp32l;
			dw_event.timeStamp32h = ((uint32)rxTimeStamp[4] << 24) + (dw_event.timeStamp32l >> 8);
                        
			instance_data.stoptimer = 1;
                        instance_data.rxTimeouts = 0;
                        instance_putevent(dw_event);
		}
                else
                {
                    instancerxon(&instance_data, 0, 0); //immediate enable
                }
	}
	else if (rxd->event == DWT_SIG_RX_TIMEOUT)
	{
		dw_event.type = DWT_SIG_RX_TIMEOUT;
		dwt_readsystime(rxTimeStamp) ;
		dw_event.timeStamp32l =  (uint32)rxTimeStamp[0] + ((uint32)rxTimeStamp[1] << 8) + ((uint32)rxTimeStamp[2] << 16) + ((uint32)rxTimeStamp[3] << 24);
		dw_event.timeStamp = rxTimeStamp[4];
		dw_event.timeStamp <<= 32;
		dw_event.timeStamp += dw_event.timeStamp32l;
		dw_event.timeStamp32h = ((uint32)rxTimeStamp[4] << 24) + (dw_event.timeStamp32l >> 8);
                dw_event.rxLength = 0;

		instance_putevent(dw_event);
	}
}

void instance_putevent(event_data_t newevent)
{
        if (newevent.type != 0)
        {
            uint8 i = 0;
            while (instance_data.dwevent[i].type != 0) i++;
            //copy new event in the end of queue
            memcpy(&instance_data.dwevent[i], &newevent, sizeof(event_data_t));
            instance_data.lastEventInd = i + 1;
        }
}

#pragma optimize=none
void instance_getevent(event_data_t* e)
{
	if(instance_data.dwevent[0].type == 0) //exit with "no event"
	{
		memset(e, 0, sizeof(event_data_t));
	}
        else
        {
            //copy the event
            memcpy(e, &instance_data.dwevent[0], sizeof(event_data_t));
            uint8 i = 0;
            while ( i < instance_data.lastEventInd)
            {
                  //shift queue
                  memcpy(&instance_data.dwevent[i], &instance_data.dwevent[i + 1], sizeof(event_data_t));
                  i++;
            }
        }
}

void instance_clearevents(void)
{
	int i = 0;

	for(i=0; i<MAX_EVENT_NUMBER; i++)
	{
            memset(&instance_data.dwevent[i], 0, sizeof(event_data_t));
	}
}

// -------------------------------------------------------------------------------------------------------------------
int instance_run(void)
{
    int done = INST_NOT_DONE_YET;

    while(done == INST_NOT_DONE_YET)
    {
        done = testapprun(&instance_data) ;     // run the communications application
    }

    
    if(done == INST_DONE_WAIT_FOR_NEXT_EVENT_TO) //we are in RX and need to timeout (Tag needs to send another poll if no Rx frame)
    {
        instance_data.instancetimer = portGetTickCnt() + DELAY; //set timeout time
        instance_data.instancetimer_en = 1; //start timer
        instance_data.stoptimer = 0 ; //clear the flag - timer can run if instancetimer_en set (set above)
    }

    //check if timer has expired
    if((instance_data.instancetimer_en == 1) && (instance_data.stoptimer == 0))
    {
        if(instance_data.instancetimer < portGetTickCnt())
        {
		event_data_t dw_event;
                dw_event.rxLength = 0;
		dw_event.type = DWT_SIG_RX_TIMEOUT;
		instance_putevent(dw_event);
        }
    }

    if (done == INST_DONE_WAIT_FOR_NEXT_EVENT) return 1;
    else return 0 ;
}


void instance_close(void)
{
    //wake up device from low power mode
    //NOTE - in the ARM  code just drop chip select for 200us
    port_SPIx_clear_chip_select();  //CS low
    usleep(200);   //200 us to wake up then waits 5ms for DW1000 XTAL to stabilise
    port_SPIx_set_chip_select();  //CS high
    msleep(85);
    dwt_entersleepaftertx(0); // clear the "enter deep sleep after tx" bit

    dwt_setinterrupt(0xFFFFFFFF, 0); //don't allow any interrupts

}


void instance_notify_DW1000_inIDLE(int idle)
{
    instance_data.dwIDLE = idle;
}
#endif


/* ==========================================================

Notes:

Previously code handled multiple instances in a single console application

Now have changed it to do a single instance only. With minimal code changes...(i.e. kept [instance] index but it is always 0.

Windows application should call instance_init() once and then in the "main loop" call instance_run().

*/
