/*! ----------------------------------------------------------------------------
 *  @file    instance.c
 *  @brief   DecaWave application level message exchange for ranging demo
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */
#include "compiler.h"
#include "port.h"
#include "deca_device_api.h"
#include "deca_regs.h"

#include "instance.h"

// -------------------------------------------------------------------------------------------------------------------

//application data message byte offsets
#define FCODE                               0               // Function code is 1st byte of messageData
#define PTXT                                1
#define RRXT                                6
#define TOFR                                1
#define RES_R1                              1               // Response option octet 0x02 (1),
#define RES_R2                              2               // Response option paramter 0x00 (1) - used to notify Tag that the report is coming
#define RES_R3                              3               // Response option paramter 0x00 (1),
#define RES_T1                              3               // Ranging request response delay low byte
#define RES_T2                              4               // Ranging request response delay high byte
#define POLL_TEMP                           1               // Poll message TEMP octet
#define POLL_VOLT                           2               // Poll message Voltage octet


// -------------------------------------------------------------------------------------------------------------------
//      Data Definitions
// -------------------------------------------------------------------------------------------------------------------
instance_state_delay_t dt;
event_data_t dw_event;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// NOTE: the maximum RX timeout is ~ 65ms
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


// -------------------------------------------------------------------------------------------------------------------
// Functions
// -------------------------------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------------------------
//
// function to construct the message/frame bytes
//
// -------------------------------------------------------------------------------------------------------------------
//
void instanceconfigframe(
#if (USING_64BIT_ADDR==1)
     srd_msg_dlsl *msg, 
#else
    srd_msg_dsss *msg,
#endif
    uint8 ackrequest, uint8 fcode)
{
    msg->panID[0] = (instance_data.panid) & 0xff;
    msg->panID[1] = instance_data.panid >> 8;

    //set frame type (0-2), SEC (3), Pending (4), ACK (5), PanIDcomp(6)
    msg->frameCtrl[0] = 0x1 /*frame type 0x1 == data*/ | 0x40 /*PID comp*/;
    msg->frameCtrl[0] |= (ackrequest ? 0x20 : 0x00);
#if (USING_64BIT_ADDR==1)
    //source/dest addressing modes and frame version
    msg->frameCtrl[1] = 0xC /*dest extended address (64bits)*/ | 0xC0 /*src extended address (64bits)*/;
    //set source address into the message structure
    memcpy(&msg->sourceAddr[0], instance_data.eui64, ADDR_BYTE_SIZE_L);
#else
    msg->frameCtrl[1] = 0x8 /*dest short address (16bits)*/ | 0x80 /*src short address (16bits)*/;
    //set source address into the message structure
    memcpy(&msg->sourceAddr[0], instance_data.eui64, ADDR_BYTE_SIZE_S);
#endif
    msg->messageData[FCODE] = fcode;
}



#if (DR_DISCOVERY == 0)
int destaddress(instance_data_t *inst)
{
    int getnext = 1;
    int tempanchorListIndex = 0;
    //set destination address (Tag will cycle though the list of anchor addresses)

    if(inst->payload.anchorPollMask == 0) return 1; //error - list is empty

    while(getnext)
    {
        if((0x1 << inst->anchorListIndex) & inst->payload.anchorPollMask)
        {
            memcpy(&inst->msg.destAddr[0], &inst->payload.anchorAddressList[inst->anchorListIndex], ADDR_BYTE_SIZE_L);
            getnext = 0;
        }

        inst->anchorListIndex++ ;
    }

    tempanchorListIndex = inst->anchorListIndex;

    while(tempanchorListIndex <= inst->payload.anchorListSize)
    {
        //check if there are any anchors left to poll
        if((0x1 << tempanchorListIndex) & inst->payload.anchorPollMask)
        {
            return 0;
        }
        tempanchorListIndex++;
    }

    //if we got this far means that we are just about to poll the last anchor in the list
    inst->anchorListIndex = 0; //start from the first anchor in the list after sleep finishes

    return 0;
}
#endif


// -------------------------------------------------------------------------------------------------------------------
//
// Turn on the receiver with/without delay
//
void instancerxon(instance_data_t *inst, int delayed, uint64 delayedReceiveTime)
{
    if (delayed)
    {
        uint32 dtime;
        dtime =  (uint32) (delayedReceiveTime>>8);
        dwt_setdelayedtrxtime(dtime) ;
    }

    if (!dwt_rxenable(delayed)) inst->rx_on = 1;  //- as when fails -1 is returned             // turn receiver on, immediate/delayed
    else inst->rx_on = 0;

} // end instancerxon()

uint32 readdevicetime(void)
{
  uint8 readbuf[5] = {0, 0, 0, 0, 0};
  uint32 devtime;
  
  dwt_readsystime(&readbuf[0]);
  devtime = (uint32)((readbuf[4] << 24) | (readbuf[3] << 16) | (readbuf[2] << 8) | readbuf[1]);
  
  return devtime;
}

// -------------------------------------------------------------------------------------------------------------------
//
// the main instance state machine (all the instance modes Tag, Anchor or Listener use the same statemachine....)
//
// -------------------------------------------------------------------------------------------------------------------
//
int testapprun(instance_data_t *inst)
{
    switch (inst->testAppState)
    {
        case TA_TXBLINK_WAIT_SEND :
        {
            int flength = (BLINK_FRAME_CRTL_AND_ADDRESS + FRAME_CRC);

            //blink frames with IEEE EUI-64 tag ID
            inst->blinkmsg.frameCtrl = 0xC5 ;
            inst->blinkmsg.seqNum = inst->frame_sn++;

            inst->testAppState = TA_TX_WAIT_CONF;             // let this state handle it
            inst->previousState = TA_TXBLINK_WAIT_SEND ;
            
            dwt_setrxtimeout((uint16)inst->timings.fwtoRnginit_sy);   //units are symbols
            dwt_writetxdata(flength, (uint8 *)  (&inst->blinkmsg), 0) ;	// write the frame data
            dwt_writetxfctrl(flength, 0);
            dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED); //always using immediate TX and enable dealyed RX
        }
        break ; // end case TA_TXBLINK_WAIT_SEND
        case TA_TXPOLL_WAIT_SEND :
        {
            //NOTE the anchor address is set after receiving the ranging initialisation message
            #if (PUT_TEMP_VOLTAGE_INTO_POLL == 1)
            {
		inst->poll_msg.messageData[POLL_TEMP] = dwt_readwakeuptemp() ;   // Temperature value set sampled at wakeup
		inst->poll_msg.messageData[POLL_VOLT] = dwt_readwakeupvbat() ;   // (Battery) Voltage value set sampled at wakeup
            }
            #endif
            
            inst->poll_msg.seqNum = inst->frame_sn++; 
            inst->psduLength = TAG_POLL_MSG_LEN;
            #if (USING_64BIT_ADDR == 1)
		inst->psduLength += FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC;
            #else
		inst->psduLength += FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC;
            #endif
            
            inst->testAppState = TA_TX_WAIT_CONF;      // wait confirmation
            inst->previousState = TA_TXPOLL_WAIT_SEND ;
            
            dwt_setrxtimeout((uint16)inst->timings.fwtoResp_sy);  //units are us - wait for 7ms after RX on (but as using delayed RX this timeout should happen at response time + 7ms)
            dwt_writetxdata(inst->psduLength, (uint8 *)  &inst->poll_msg, 0) ;	// write the frame data
            dwt_writetxfctrl(inst->psduLength, 0);
            dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);
        }
        break;
        case TA_TXFINAL_WAIT_SEND :
        {
            // Embbed into Final message: 40-bit pollTXTime,  40-bit respRxTime
            // Write Poll TX time field of Final message
            memcpy(&(inst->final_msg.messageData[PTXT]), (uint8 *)&dt.DT_TX_WAIT_CONF.TX_POLL, 5);
            // Write Response RX time field of Final message
            memcpy(&(inst->final_msg.messageData[RRXT]), (uint8 *)&dt.DT_RX_WAIT_DATA.RX_RESP, 5);
            inst->final_msg.seqNum = inst->frame_sn++;
            
            inst->psduLength = TAG_FINAL_MSG_LEN;
            #if (USING_64BIT_ADDR == 1)
		inst->psduLength += FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC;
            #else
		inst->psduLength += FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC;
            #endif
                
            inst->testAppState = TA_TX_WAIT_CONF;               // wait confirmation
            inst->previousState = TA_TXFINAL_WAIT_SEND;
            
            dwt_writetxdata(inst->psduLength, (uint8 *)  &inst->final_msg, 0) ;	// write the frame data
            dwt_writetxfctrl(inst->psduLength, 0); 
            #if (USING_DELAYED_TRX == 1)
                //calculate delay for delayed transmission
                inst->txDelay = readdevicetime() + inst->timings.ftdFinal_tu;
                dwt_setdelayedtrxtime(inst->txDelay);                    
                dwt_starttx(DWT_START_TX_DELAYED | DWT_RESPONSE_EXPECTED);        //not in this case!!
            #else
                dwt_starttx(DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED);
            #endif
        }
        break;
        case TA_TX_WAIT_CONF :
        {
            instance_getevent(&dw_event); //get and clear this event

            //NOTE: Can get the ACK before the TX confirm event for the frame requesting the ACK
            //this happens because if polling the ISR the RX event will be processed 1st and then the TX event
            //thus the reception of the ACK will be processed before the TX confirmation of the frame that requested it.
            if (dw_event.type != DWT_SIG_TX_DONE) //wait for TX done confirmation
            {
                //we need to wait for SIG_TX_DONE and then process the timeout and re-send the frame if needed
                if (dw_event.type != 0) instance_putevent(dw_event);
                break;
            }

            if (inst->previousState == TA_TXBLINK_WAIT_SEND)
            {
                dt.DT_TX_WAIT_CONF.TX_BLINK = dw_event.timeStamp;
                inst->testAppState = TA_RXE_WAIT;
            }
            else if (inst->previousState == TA_TXPOLL_WAIT_SEND)
            {
                dt.DT_TX_WAIT_CONF.TX_POLL = dw_event.timeStamp - dt.DT_TX_WAIT_CONF.TX_BLINK;
                inst->testAppState = TA_RXE_WAIT;
            }
            else 
            {
                inst->done = INST_DONE_WAIT_FOR_NEXT_EVENT;
                inst->testAppState = TA_TXPOLL_WAIT_SEND;
            }
        }
        break;
        case TA_RXE_WAIT :
        {
            //turn RX on
            instancerxon(inst, 0, 0) ;   // turn RX on, with/without delay

            inst->testAppState = TA_RX_WAIT_DATA;   // let this state handle it
            // end case TA_RXE_WAIT, don't break, but fall through into the TA_RX_WAIT_DATA state to process it immediately.
        }
        case TA_RX_WAIT_DATA :                                                                     // Wait RX data
        {
            instance_getevent(&dw_event);                                    //get and clear last event
            switch (dw_event.type)
            {
                //if we have received a DWT_SIG_RX_OKAY event - this means that the message is IEEE data type - need to check frame control to know which addressing mode is used
                case DWT_SIG_RX_OKAY :
                {
                    int fn_code = 0;
                    uint8 *messageData;
                    inst->stoptimer = 0; //clear the flag, as we have received a message
                    inst->connect = 1;

                    fn_code = dw_event.msgu.rxmsg.messageData[FCODE];
                    messageData = &dw_event.msgu.rxmsg.messageData[0];
                                            
                    switch(fn_code)
                    {
                            case RTLS_DEMO_MSG_RNG_INIT:
                            {
                                if(inst->mode == TAG_TDOA) //only start ranging with someone if not ranging already
                                {
                                    if (inst->Anchor_list_len < ANCHOR_LIST_SIZE)
                                    {
                                        #if (ENABLE_FRAME_FILTERING == 1)
                                            dwt_enableframefilter(DWT_FF_DATA_EN);     //allow data frames
                                        #endif
                                    
                                        //add this Anchor to the list of Anchors we know about
                                        instaddanchortolist(&(dw_event.msgu.rxmsg.sourceAddr[0]));
                                        //set new addres for poll message
                                        instselectanchor();
                                        #if (USING_64BIT_ADDR == 0)
                                            inst->tagShortAdd = messageData[RES_R1] + (messageData[RES_R2] << 8);
                                            memcpy(&inst->poll_msg.sourceAddr[0], &inst->tagShortAdd, ADDR_BYTE_SIZE_S);                 //set the new tag address assigned by anchor (set source address)
                                            memcpy(&inst->final_msg.sourceAddr[0], &inst->tagShortAdd, ADDR_BYTE_SIZE_S); 
                                        #endif

                                        inst->mode = TAG;
                                        inst->testAppState = TA_TXPOLL_WAIT_SEND;
                                    }
                                    //else ignore new ranging_init message
                                }
				//else we ignore this message if already associated... (not TAG_TDOA)
                            }
                            break; //RTLS_DEMO_MSG_RNG_INIT
                            case RTLS_DEMO_MSG_ANCH_RESP:
                            {
                                if (memcmp(&instance_data.poll_msg.destAddr[0], &dw_event.msgu.rxmsg.sourceAddr[0], 8) == 0)
                                {
                                    dt.DT_RX_WAIT_DATA.RX_RESP = dw_event.timeStamp - dt.DT_TX_WAIT_CONF.TX_BLINK;
                                                            
                                    #if (USING_64BIT_ADDR == 1)
                                        memcpy(&instance_data.final_msg.destAddr[0], &instance_data.poll_msg.destAddr[0], ADDR_BYTE_SIZE_L); //copy long dest address of final message
                                    #else
                                        memcpy(&instance_data.final_msg.destAddr[0], &instance_data.poll_msg.destAddr[0], ADDR_BYTE_SIZE_S); //copy short dest address of final message
                                    #endif

                                    inst->testAppState = TA_TXFINAL_WAIT_SEND ; // send our response / the final
                                }
                            }
                            break; //RTLS_DEMO_MSG_ANCH_RESP
                            default:
                            {
                                //only enable receiver when not using double buffering
                                inst->testAppState = TA_RXE_WAIT ;              // wait for next frame
                                dwt_setrxaftertxdelay(DISABLE);
                            }
                            break;
			} //end switch (fcode)
                }
		break ; //end of DWT_SIG_RX_OKAY
                case DWT_SIG_RX_TIMEOUT :
                {
                    inst->rxTimeouts ++ ;
                    inst->connect = 0;
                    // initiate the re-transmission of the poll that was not responded to
                    if (inst->rxTimeouts >= MAX_NUMBER_OF_POLL_RETRYS) //if no response after sending 20 Polls - go back to blink mode
                    {
                        if (inst->previousState == TA_TXPOLL_WAIT_SEND)
                        {
                            #if (ENABLE_FRAME_FILTERING == 1)
                                dwt_enableframefilter(DWT_FF_NOTYPE_EN); //disable frame filtering
                            #endif
                            inst->testAppState = TA_TXBLINK_WAIT_SEND;
                            inst->mode = TAG_TDOA;
                            inst->rxTimeouts = 0;
                        }
                        else inst->testAppState = inst->previousState;
                    }
                    else 
                    {
                        if (inst->previousState == TA_TXPOLL_WAIT_SEND) instselectanchor();
                        inst->testAppState = inst->previousState;
                    }
    
                    //timeout - disable the radio (if using SW timeout the rx will not be off)
                    dwt_forcetrxoff() ;
                    inst->rx_on = 0;
                }
                break ;
                case DWT_SIG_TX_AA_DONE: //ignore this event - just process the rx frame that was received before the ACK response
		case 0:
		default :
                {
                    if(inst->done == INST_NOT_DONE_YET) inst->done = INST_DONE_NO_EVENT;
                }
                break;
            }
            break ; // end case TA_RX_WAIT_DATA
            default:
                //ERROR
            break;
        }
    } // end switch on testAppState

    return inst->done;
} // end testapprun()

// -------------------------------------------------------------------------------------------------------------------
#if NUM_INST != 1
#error These functions assume one instance only
#else


// -------------------------------------------------------------------------------------------------------------------
// function to initialise instance structures
//
// Returns 0 on success and -1 on error
int instance_init_s(int mode)
{
    instance_data.mode =  mode;                                // assume anchor,
    instance_data.testAppState = TA_INIT ;

    // if using auto CRC check (DWT_INT_RFCG and DWT_INT_RFCE) are used instead of DWT_INT_RDFR flag
    // other errors which need to be checked (as they disable receiver) are
    //dwt_setinterrupt(DWT_INT_TFRS | DWT_INT_RFCG | (DWT_INT_ARFE | DWT_INT_RFSL | DWT_INT_SFDT | DWT_INT_RPHE | DWT_INT_RFCE | DWT_INT_RFTO /*| DWT_INT_RXPTO*/), 1);
    dwt_setinterrupt(DWT_INT_TFRS | DWT_INT_RFCG | DWT_INT_RFTO, 1);
    //dwt_setinterrupt(DWT_INT_ALL, 1);

    dwt_setcallbacks(instance_txcallback, instance_rxcallback);

    instance_data.doublebufferon = DOUBLE_RX_BUFFER;

    dwt_seteui(instance_data.eui64);
    dwt_setpanid(instance_data.panid);
    instance_settrxtimeouts(FIXED_REPLY_DELAY);
    dwt_enableframefilter(DWT_FF_NOTYPE_EN); //disable frame filtering
    //do not use a delayed receive
    dwt_setrxaftertxdelay(DISABLE);
    dwt_setautorxreenable(instance_data.rxautoreenable); //not necessary to auto RX re-enable as the receiver is on for a short time (Tag knows when the response is coming)
    dwt_setdblrxbuffmode(instance_data.doublebufferon); //disable double RX buffer

    #if (ENABLE_AUTO_ACK == 1) //NOTE - Auto ACK only works if frame filtering is enabled!
        dwt_enableautoack(ACK_RESPONSE_TIME); //wait for ACK_RESPONSE_TIME symbols (e.g. 5) before replying with the ACK
    #endif
    memcpy(instance_data.blinkmsg.tagID, instance_data.eui64, ADDR_BYTE_SIZE_L);
    instanceconfigframe(&instance_data.poll_msg, !ACK_REQUESTED, RTLS_DEMO_MSG_TAG_POLL);
    instanceconfigframe(&instance_data.final_msg, !ACK_REQUESTED, RTLS_DEMO_MSG_TAG_FINAL);
                        
    instance_data.mode = TAG_TDOA ;
    //start the lifecycle
    instance_data.testAppState = TA_TXBLINK_WAIT_SEND;
    return 0 ;
}

// -------------------------------------------------------------------------------------------------------------------
// function to set the fixed reply delay time (in ms)
//
extern uint8 dwnsSFDlen[];

uint32 instanceframeduration(int datalength) //duration in symbols
{
	//configure the rx delay receive delay time, it is dependent on the message length
	float msgdatalen = 0;
	float preamblelen = 0;
	int sfdlen = 0;
	int x = 0;

	msgdatalen = (float) datalength;

	x = (int) ceil(msgdatalen*8/330.0f);

	msgdatalen = msgdatalen*8 + x*48;

	//assume PHR length is 172308us for 110k and 21539us for 850k/6.81M
	if(instance_data.configData.dataRate == DWT_BR_110K)
	{
		msgdatalen *= 8205.13f;
		msgdatalen += 172308;

	}
	else if(instance_data.configData.dataRate == DWT_BR_850K)
	{
		msgdatalen *= 1025.64f;
		msgdatalen += 21539;
	}
	else
	{
		msgdatalen *= 128.21f;
		msgdatalen += 21539;
	}

	//SFD length is 64 for 110k (always)
	//SFD length is 8 for 6.81M, and 16 for 850k, but can vary between 8 and 16 bytes
	sfdlen = dwnsSFDlen[instance_data.configData.dataRate];

	switch (instance_data.configData.txPreambLength)
    {
    case DWT_PLEN_4096 : preamblelen = 4096.0f; break;
    case DWT_PLEN_2048 : preamblelen = 2048.0f; break;
    case DWT_PLEN_1536 : preamblelen = 1536.0f; break;
    case DWT_PLEN_1024 : preamblelen = 1024.0f; break;
    case DWT_PLEN_512  : preamblelen = 512.0f; break;
    case DWT_PLEN_256  : preamblelen = 256.0f; break;
    case DWT_PLEN_128  : preamblelen = 128.0f; break;
    case DWT_PLEN_64   : preamblelen = 64.0f; break;
    }

	//preamble  = plen * (994 or 1018) depending on 16 or 64 PRF
	if(instance_data.configData.prf == DWT_PRF_16M)
	{
		preamblelen = (sfdlen + preamblelen) * 0.99359f;
	}
	else
	{
		preamblelen = (sfdlen + preamblelen) * 1.01763f;
	}


	//total time the frame takes in symbols
	return (16 + (int)((preamblelen + (msgdatalen/1000.0))/ 1.0256));

}

// -------------------------------------------------------------------------------------------------------------------
// function to set delays between sending and receiving messages
void instance_settrxtimeouts(int delayms) 
{
    int msgdatalen = 0;
    uint32 to = 0;
    
    //calc blink message duration
    msgdatalen = BLINK_FRAME_CRTL_AND_ADDRESS + FRAME_CRC;
    instance_data.timings.flBlink_sy = instanceframeduration(msgdatalen);
    
    //calc ranging init message duration, rx_timeout and tx_delay
#if (USING_64BIT_ADDR == 1)
    msgdatalen = RANGINGINIT_MSG_LEN + FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC;
#else
    msgdatalen = RANGINGINIT_MSG_LEN + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC;
#endif
    instance_data.timings.flRnginit_sy = instanceframeduration(msgdatalen);
    to = (uint32)(delayms * 1000 / 1.0256) + instance_data.timings.flRnginit_sy;
    if (to > 0xffff) instance_data.timings.fwtoRnginit_sy = 0xffff;
    else instance_data.timings.fwtoRnginit_sy = to;
#if (USING_DELAYED_TRX == 1)    
    if(instance_data.configData.dataRate == DWT_BR_110K)
    {
        instance_data.timings.ftdRnginit_tu = convertmicrosectodevicetimeu32((double)(instance_data.timings.flBlink_sy / 15.65f));
    }
    else
    {
      instance_data.timings.ftdRnginit_tu = convertmicrosectodevicetimeu32((double)(instance_data.timings.flBlink_sy / 62.6f));
    }
#endif
    
    //calc poll message duration
#if (USING_64BIT_ADDR == 1)
    msgdatalen = TAG_POLL_MSG_LEN + FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC;
#else
    msgdatalen = TAG_POLL_MSG_LEN + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC;
#endif
    instance_data.timings.flPoll_sy = instanceframeduration(msgdatalen);
    to = (uint32)(delayms * 1000 / 1.0256) + instance_data.timings.flPoll_sy;
    if (to > 0xffff) instance_data.timings.fwtoPoll_sy = 0xffff;
    else instance_data.timings.fwtoPoll_sy = to;
    
    //calc response message duration, rx_timeout and tx_delay
#if (USING_64BIT_ADDR == 1)
    msgdatalen = ANCH_RESPONSE_MSG_LEN + FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC;
#else
    msgdatalen = ANCH_RESPONSE_MSG_LEN + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC;
#endif
    instance_data.timings.flResp_sy = instanceframeduration(msgdatalen);
    to = (uint32)(delayms * 1000 / 1.0256) + instance_data.timings.flResp_sy;
    if (to > 0xffff) instance_data.timings.fwtoResp_sy = 0xffff;
    else instance_data.timings.fwtoResp_sy = to;
#if (USING_DELAYED_TRX == 1)    
    if(instance_data.configData.dataRate == DWT_BR_110K)
    {
        instance_data.timings.ftdResp_tu = convertmicrosectodevicetimeu32((double)(instance_data..timings.flPoll_sy / 15.65f));
    }
    else
    {
        instance_data.timings.ftdResp_tu = convertmicrosectodevicetimeu32((double)(instance_data.timings.flPoll_sy / 62.6f));
    }
#endif
    
    //calc final message duration, rx_timeout and tx_delay
#if (USING_64BIT_ADDR == 1)
    msgdatalen = TAG_FINAL_MSG_LEN + FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC;
#else
    msgdatalen = TAG_FINAL_MSG_LEN + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC;
#endif
    instance_data.timings.flFinal_sy = instanceframeduration(msgdatalen);
    to = (uint32)(delayms * 1000 / 1.0256) + instance_data.timings.flFinal_sy;
    if (to > 0xffff) instance_data.timings.fwtoFinal_sy = 0xffff;
    else instance_data.timings.fwtoFinal_sy = to;
#if (USING_DELAYED_TRX == 1)
    if(instance_data.configData.dataRate == DWT_BR_110K)
    {
        instance_data.timings.ftdFinal_tu = convertmicrosectodevicetimeu32((double)(instance_data.timings.flResp_sy / 15.65f));
    }
    else
    {
        instance_data.timings.ftdFinal_tu = convertmicrosectodevicetimeu32((double)(instance_data.timings.flResp_sy / 62.6f));
    }
#endif
}

#if (DR_DISCOVERY == 0)
// -------------------------------------------------------------------------------------------------------------------
//
// Set Payload parameters for the instance
//
// -------------------------------------------------------------------------------------------------------------------
void instancesetaddresses(instanceAddressConfig_t *plconfig)
{
    instance_data.payload = *plconfig ;       // copy configurations

    instancesetreporting(instance_data.payload.sendReport);

}
#endif

uint64 instance_get_addr(void) //get own address
{
    uint64 x = (uint64) instance_data.eui64[0];
    x |= (uint64) instance_data.eui64[1] << 8;
    x |= (uint64) instance_data.eui64[2] << 16;
    x |= (uint64) instance_data.eui64[3] << 24;
    x |= (uint64) instance_data.eui64[4] << 32;
    x |= (uint64) instance_data.eui64[5] << 40;
    x |= (uint64) instance_data.eui64[6] << 48;
    x |= (uint64) instance_data.eui64[7] << 56;


    return (x);
}

void instance_readaccumulatordata(void)
{
#if DECA_SUPPORT_SOUNDING==1
    uint16 len = 992 ; //default (16M prf)

    if (instance_data.configData.prf == DWT_PRF_64M)  // Figure out length to read
        len = 1016 ;

    instance_data.buff.accumLength = len ;                                       // remember Length, then read the accumulator data

    len = len*4+1 ;   // extra 1 as first byte is dummy due to internal memory access delay

    dwt_readaccdata((uint8*)&(instance_data.buff.accumData->dummy), len, 0);
#endif  // support_sounding
}

#endif


/* ==========================================================

Notes:

Previously code handled multiple instances in a single console application

Now have changed it to do a single instance only. With minimal code changes...(i.e. kept [instance] index but it is always 0.

Windows application should call instance_init() once and then in the "main loop" call instance_run().

*/
