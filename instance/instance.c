/*! ----------------------------------------------------------------------------
 *  @file    instance.c
 *  @brief   DecaWave application level message exchange for ranging demo
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */
#include "compiler.h"
#include "port.h"
#include "deca_device_api.h"
#include "deca_regs.h"
#include "instance.h"
#if defined(STM32F411xE)
  #include "mtwist.h"
#endif

#define FCODE                               0               // Function code is 1st byte of messageData
#define X                                   1               // tag-anchor x coord starts at 2nd byte of messageData
#define Y                                   9               // tag-anchor y coord starts at 9rd byte of messageData
#define PLRS                                17              // anchor poll-response time starts at 17nd byte of messageData

// -------------------------------------------------------------------------------------------------------------------
//      Data Definitions
// -------------------------------------------------------------------------------------------------------------------
//The table below specifies the default TX spectrum configuration parameters... this has been tuned for DW EVK hardware units
//the table is set for smart power - see below in the instance_config function how this is used when not using smart power
const tx_struct txSpectrumConfig[8] =
{
	//Channel 0 ----- this is just a place holder so the next array element is channel 1
    {
            0x0,   //0
            {
                    0x0, //0
                    0x0 //0
            }
    },
    //Channel 1
    {
			0xc9,   //PG_DELAY
            {
                    0x15355575, //16M prf power
                    0x07274767 //64M prf power
            }

    },
    //Channel 2
    {
            0xc2,   //PG_DELAY
            {
                    0x15355575, //16M prf power
                    0x07274767 //64M prf power
            }
    },
    //Channel 3
    {
            0xc5,   //PG_DELAY
            {
                    0x0f2f4f6f, //16M prf power
                    0x2b4b6b8b //64M prf power
            }
    },
    //Channel 4
    {
            0x95,   //PG_DELAY
            {
                    0x1f1f3f5f, //16M prf power
                    0x3a5a7a9a //64M prf power
            }
    },
    //Channel 5
    {
            0xc0,   //PG_DELAY
            {
                    0x0E082848, //16M prf power
                    0x25456585 //64M prf power
            }
    },
    //Channel 6 ----- this is just a place holder so the next array element is channel 7
    {
            0x0,   //0
            {
                    0x0, //0
                    0x0 //0
            }
    },
    //Channel 7
    {
            0x93,   //PG_DELAY
            {
                    0x32527292, //16M prf power
                    0x5171B1d1 //64M prf power
            }
    }
};

//these are default antenna delays for EVB1000, these can be used if there is no calibration data in the DW1000,
//or instead of the calibration data
const uint16 rfDelays[2] = {
		(uint16) ((DWT_PRF_16M_RFDLY/ 2.0) * 1e-9 / DWT_TIME_UNITS),//PRF 16
		(uint16) ((DWT_PRF_64M_RFDLY/ 2.0) * 1e-9 / DWT_TIME_UNITS)
};

#if defined(STM32F411xE)
    //initialize MTwist RNG
    mt_state mtwist;
    uint32_t s[MT_STATE_SIZE];
#endif
//timings
event_data_t dw_event;
//instance handler
instance_data_t instance_data;

// -------------------------------------------------------------------------------------------------------------------
// Functions
// -------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------
// convert microseconds to device time
uint64 convertmicrosectodevicetimeu (double microsecu)
{
    uint64 dt;
    long double dtime;

    dtime = (microsecu / (double) DWT_TIME_UNITS) / 1e6 ;

    dt =  (uint64) (dtime) ;

    return dt;
}

uint32 convertmicrosectodevicetimeu32 (double microsecu)
{
    uint32 dt;
    long double dtime;

    dtime = (microsecu / (double) DWT_TIME_UNITS) / 1e6 ;

    dt =  (uint32) (dtime) ;

    return dt;
}


double convertdevicetimetosec(int32 dt)
{
    double f = 0;

    f =  dt * DWT_TIME_UNITS ;  // seconds #define TIME_UNITS          (1.0/499.2e6/128.0) = 15.65e-12

    return f ;
}

double convertdevicetimetosec8(uint8* dt)
{
    double f = 0;

    uint32 lo = 0;
    int8 hi = 0;

    memcpy(&lo, dt, 4);
    hi = dt[4] ;

    f = ((hi * 0x100000000) + lo) * DWT_TIME_UNITS ;  // seconds #define TIME_UNITS          (1.0/499.2e6/128.0) = 15.65e-12

    return f ;
}

//--------------------------------------------------------------------------------
// ToF and distances
measure_t reportTOF(void)
{
    int64 tofi;
    measure_t res = {-1, -1, 0, 0};
    double distance, velocity, bias, noise ;
    double tof ;

    tofi = instance_data.Ancs[instance_data.ai].ToF.RX_RESP - 
           instance_data.Ancs[instance_data.ai].ToF.TX_RESP;
    // check for negative results and accept them making them proper negative integers
    if (tofi > 0x007FFFFFFFFF)                          // MP counter is 40 bits,  close up TOF may be negative
    {
        tofi -= 0x010000000000 ;                        // subtract fill 40 bit range to make it negative
    }
    tofi >>= 1;
    // convert to seconds (as floating point)
    tof = convertdevicetimetosec(tofi);                 //this is divided by 2 rounds - to get single time of flight
    distance = tof * SPEED_OF_LIGHT;

    if ((distance < -0.5) || (distance > 20000.000))    // discard any results less than <50 cm or >20km
        return res;

#if (CORRECT_RANGE_BIAS == 1)
    bias = dwt_getrangebias(instance_data.configData.chan, (float) distance, instance_data.configData.prf);
    distance = distance + bias;
    dwt_readdignostics(&instance_data.diagRX);
    noise = instance_data.diagRX.stdNoise; 
#endif
    instance_data.Ancs[instance_data.ai].dt = portGetTickCnt() - instance_data.Ancs[instance_data.ai].dt; // get time between ranges
    distance = fabs(distance);                          // make any (small) negatives positive.
    velocity = (distance - instance_data.Ancs[instance_data.ai].measures.range) / (double)(instance_data.Ancs[instance_data.ai].dt * 1e-6);
    res.range = distance; res.vel = velocity; res.bias = bias; res.noise = noise;
    
    return res;
}// end of reportTOF

bool instancenewrange(void) //toggle then new range occors
{
    if (instance_data.newrange)
    {
        instance_data.newrange = false;
        return true;
    }

    return false;
}

// -------------------------------------------------------------------------------------------------------------------
//
// function to allow application configuration be passed into instance and affect underlying device opetation
//
void instance_config(instanceConfig_t *config, uint8 manualpoweren)
{
    int use_otpdata = DWT_LOADANTDLY | DWT_LOADXTALTRIM;
    uint32 power = 0;

    instance_data.configData.chan = config->channelNumber ;
    instance_data.configData.trxCode =  config->preambleCode ;
    instance_data.configData.prf = config->pulseRepFreq ;
    instance_data.configData.dataRate = config->dataRate ;
    instance_data.configData.txPreambLength = config->preambleLen ;
    instance_data.configData.rxPAC = config->pacSize ;
    instance_data.configData.nsSFD = config->nsSFD ;
    instance_data.configData.sfdTO = config->sfdTO;
    instance_data.configData.phrMode = config->phrMode;
    instance_data.configData.smartPowerEn = 0;
    //configure the channel parameters
    dwt_configure(&instance_data.configData, use_otpdata) ;

    instance_data.configTX.PGdly = txSpectrumConfig[config->channelNumber].PGdelay ;

   //Configure TX power
    if (manualpoweren)
    {
        power = 0x1f;
    }
    else
    {
        //firstly check if there are calibrated TX power value in the DW1000 OTP
        power = dwt_getotptxpower(config->pulseRepFreq, instance_data.configData.chan);

        if((power == 0x0) || (power == 0xFFFFFFFF)) //if there are no calibrated values... need to use defaults
        {
            power = txSpectrumConfig[config->channelNumber].txPwr[config->pulseRepFreq - DWT_PRF_16M];
        }
    }
    //if smart power is used then the value as read from OTP is used directly
    //if smart power is used the user needs to make sure to transmit only one frame per 1ms or TX spectrum power will be violated
    if(instance_data.configData.smartPowerEn)
    {
        instance_data.configTX.power = power;
    }
    else //if the smart power is not used, then the low byte value (repeated) is used for the whole TX power register
    {
        uint8 pow = power & 0xFF;
        instance_data.configTX.power = (pow | (pow << 8) | (pow << 16) | (pow << 24));
    }
    dwt_setsmarttxpower(instance_data.configData.smartPowerEn);

    //configure the tx spectrum parameters (power and PG delay)
    dwt_configuretxrf(&instance_data.configTX);

    //check if to use the antenna delay calibration values as read from the OTP
    if((use_otpdata & DWT_LOADANTDLY) == 0)
    {
        instance_data.txantennaDelay = rfDelays[config->pulseRepFreq - DWT_PRF_16M];
        // -------------------------------------------------------------------------------------------------------------------
        // set the antenna delay, we assume that the RX is the same as TX.
        dwt_setrxantennadelay(instance_data.txantennaDelay);
        dwt_settxantennadelay(instance_data.txantennaDelay);
    }
    else
    {
        //get the antenna delay that was read from the OTP calibration area
        instance_data.txantennaDelay = dwt_readantennadelay(config->pulseRepFreq) >> 1;

        // if nothing was actually programmed then set a reasonable value anyway
        if (instance_data.txantennaDelay == 0)
        {
            instance_data.txantennaDelay = rfDelays[config->pulseRepFreq - DWT_PRF_16M];
            // -------------------------------------------------------------------------------------------------------------------
            // set the antenna delay, we assume that the RX is the same as TX.
            dwt_setrxantennadelay(instance_data.txantennaDelay);
            dwt_settxantennadelay(instance_data.txantennaDelay);
        }
    }

    if(config->preambleLen == DWT_PLEN_64) //if preamble length is 64
    {
        SPI_ChangeRate(DW1000_SPI, SPI_BaudRatePrescaler_32); //reduce SPI to < 3MHz
	dwt_loadopsettabfromotp(0);
	SPI_ChangeRate(DW1000_SPI, SPI_BaudRatePrescaler_4); //increase SPI to max
    }
}

// -------------------------------------------------------------------------------------------------------------------
// message trx callbacks
void instance_txcallback(const dwt_callback_data_t *txd)
{
	uint8 txTimeStamp[5] = {0, 0, 0, 0, 0};

	uint8 txevent = txd->event;
	event_data_t dw_event;

	if(txevent == DWT_SIG_TX_DONE)
	{
		dwt_readtxtimestamp(txTimeStamp) ;
		dw_event.timeStamp32l = (uint32)txTimeStamp[0] + ((uint32)txTimeStamp[1] << 8) + ((uint32)txTimeStamp[2] << 16) + ((uint32)txTimeStamp[3] << 24);
		dw_event.timeStamp = txTimeStamp[4];
                dw_event.timeStamp <<= 32;
		dw_event.timeStamp += dw_event.timeStamp32l;
		dw_event.timeStamp32h = ((uint32)txTimeStamp[4] << 24) + (dw_event.timeStamp32l >> 8);
                dw_event.rxLength = 0;
		dw_event.type = DWT_SIG_TX_DONE ;
                
		instance_putevent(&dw_event);

	}
	else if(txevent == DWT_SIG_TX_AA_DONE)
	{
		//auto ACK confirmation
		dw_event.rxLength = 0;
		dw_event.type = DWT_SIG_TX_AA_DONE ;

		instance_putevent(&dw_event);
	}
}

void instance_rxcallback(const dwt_callback_data_t *rxd)
{
	uint8 rxTimeStamp[5]  = {0, 0, 0, 0, 0};

        uint8 rxd_event = 0;
	event_data_t dw_event;

	//if we got a frame with a good CRC - RX OK
        if(rxd->event == DWT_SIG_RX_OKAY)
	{
                //the overrun has occured while we were reading the data - dump the frame/data
                if(dwt_checkoverrun()) 
                {
                    instancerxon(&instance_data, false, 0); //immediate enable
                    return;
                }
                  
                rxd_event = DWT_SIG_RX_OKAY;
                dw_event.rxLength = rxd->datalength;

		//need to process the frame control bytes to figure out what type of frame we have received
                switch(rxd->fctrl[0])
                {
                    //blink type frame
		    case 0x00:
                    //data type frames (with/without ACK request) - assume PIDC is on.
                    case 0x41:
                    case 0x61:
                            //read the frame
                            if(rxd->datalength > STANDARD_FRAME_SIZE) rxd_event = DWT_SIG_RX_UNKNOWN;
                            break;
                    //any other frame types are not supported by this application
                    default:
                            rxd_event = DWT_SIG_RX_UNKNOWN;
                            break;
		}

                dw_event.type = rxd_event;
                //read rx timestamp
		if(rxd_event == DWT_SIG_RX_OKAY)
		{
			dwt_readrxtimestamp(rxTimeStamp) ;
			dw_event.timeStamp32l =  (uint32)rxTimeStamp[0] + ((uint32)rxTimeStamp[1] << 8) + ((uint32)rxTimeStamp[2] << 16) + ((uint32)rxTimeStamp[3] << 24);
			dw_event.timeStamp = rxTimeStamp[4];
			dw_event.timeStamp <<= 32;
			dw_event.timeStamp += dw_event.timeStamp32l;
			dw_event.timeStamp32h = ((uint32)rxTimeStamp[4] << 24) + (dw_event.timeStamp32l >> 8);
                        
                        dwt_readrxdata((uint8 *)&dw_event.msgu.frame[0], rxd->datalength, 0);  // Read Data Frame
		}
	}
	else if (rxd->event == DWT_SIG_RX_TIMEOUT) dw_event.type = DWT_SIG_RX_TIMEOUT;

        instance_putevent(&dw_event);
}

//--------------------------------------------------------------------------------
// functions of event queue routine
void instance_putevent(event_data_t* newevent)
{
        if (newevent->type != 0)
        {
            uint8 i = 0;
            while (instance_data.dwevent[i].type != 0) i++;
            //copy new event in the end of queue
            memcpy(&instance_data.dwevent[i], newevent, sizeof(event_data_t));
            instance_data.lastEventInd = i + 1;
        }
}

#pragma optimize=none
void instance_getevent(event_data_t* e)
{
	if(instance_data.dwevent[0].type == 0) //exit with "no event"
	{
		memset(e, 0, sizeof(event_data_t));
	}
        else
        {
            //copy the event
            memcpy(e, &instance_data.dwevent[0], sizeof(event_data_t));
            uint8 i = 0;
            while ( i < instance_data.lastEventInd)
            {
                  //shift queue
                  memcpy(&instance_data.dwevent[i], &instance_data.dwevent[i + 1], sizeof(event_data_t));
                  i++;
            }
        }
}

void instance_clearevents(void)
{
	int i = 0;

	for(i = 0; i < MAX_EVENT_NUMBER; i++)
	{
            memset(&instance_data.dwevent[i], 0, sizeof(event_data_t));
	}
        instance_data.lastEventInd = 0;
}

// -------------------------------------------------------------------------------------------------------------------
// function to construct the message/frame bytes
//
#define frCtrl0  0
#define frCtrl1  1
#define panID0   3
#define panID1   4
void instanceconfigframe(uint8 *msg, uint8 fcode)
{
    memset(msg, 0, STANDARD_FRAME_SIZE);
    
    msg[panID0] = (instance_data.panid) & 0xff;
    msg[panID1] = instance_data.panid >> 8;

    if (fcode == RTLS_DEMO_MSG_RESP) 
    {
        msg[frCtrl0] = 0x1 /*frame type 0x1 == data*/ | 0x40 /*PID comp*/;
        msg[frCtrl1] = 0xC /*dest extended address (64bits)*/ | 0xC0 /*src extended address (64bits)*/;
        //set source address into the message structure
        memcpy(&msg[FRAME_CTRLP + ADDR_BYTE_SIZE_L], &instance_data.eui[0], ADDR_BYTE_SIZE_L);
        msg[FRAME_CRTL_AND_ADDRESS_L] = fcode;
    }
    else
    {
        msg[frCtrl0] = 0x0 /*frame type 0x0 == beacon*/ | 0x00 /*PID comp*/;
        msg[frCtrl1] = 0x0 /*no dest address*/ | 0xC0 /*src extended address (64bits)*/;
        //set source address into the message structure
        memcpy(&msg[FRAME_CTRLP], &instance_data.eui[0], ADDR_BYTE_SIZE_L);
        msg[FRAME_CRTL_AND_ADDRESS_LN] = fcode;
    }
}

// -------------------------------------------------------------------------------------------------------------------
// Read the DW1000 IC time
uint64 readdevicetime(void)
{
  uint8 readbuf[5] = {0, 0, 0, 0, 0};
  uint64 devtime = 0;
  
  dwt_readsystime(&readbuf[0]);
  devtime = (uint32)((readbuf[4] << 24) | (readbuf[3] << 16) | (readbuf[2] << 8) | readbuf[1]);
  devtime <<= 8;
  devtime |=  readbuf[0];
  
  return devtime;
}

// -------------------------------------------------------------------------------------------------------------------
// Turn on the receiver with/without delay
void instancerxon(instance_data_t *inst, int delayed, uint64 delayedReceiveTime)
{
    if (delayed)
    {
        uint32 dtime;
        dtime = (uint32)(delayedReceiveTime>>8);
        dwt_setdelayedtrxtime(dtime) ;
    }

    dwt_rxenable(delayed);  // turn receiver on, immediate/delayed
} // end instancerxon()

// -------------------------------------------------------------------------------------------------------------------
//
// Calculate the frame duration in symbols (with preamble)
uint32 instanceframeduration(int datalength)
{
	//configure the rx delay receive delay time, it is dependent on the message length
	float msgdatalen = 0;
	float preamblelen = 0;
	int sfdlen = 0;
	int x = 0;

	msgdatalen = (float) datalength;

	x = (int) ceil(msgdatalen*8/330.0f);

	msgdatalen = msgdatalen*8 + x*48;

	//assume PHR length is 172308us for 110k and 21539us for 850k/6.81M
	if(instance_data.configData.dataRate == DWT_BR_110K)
	{
		msgdatalen *= 8205.13f;
		msgdatalen += 172308;

	}
	else if(instance_data.configData.dataRate == DWT_BR_850K)
	{
		msgdatalen *= 1025.64f;
		msgdatalen += 21539;
	}
	else
	{
		msgdatalen *= 128.21f;
		msgdatalen += 21539;
	}

	//SFD length is 64 for 110k (always)
	//SFD length is 8 for 6.81M, and 16 for 850k, but can vary between 8 and 16 bytes
	sfdlen = dwnsSFDlen[instance_data.configData.dataRate];

	switch (instance_data.configData.txPreambLength)
        {
              case DWT_PLEN_4096 : preamblelen = 4096.0f; break;
              case DWT_PLEN_2048 : preamblelen = 2048.0f; break;
              case DWT_PLEN_1536 : preamblelen = 1536.0f; break;
              case DWT_PLEN_1024 : preamblelen = 1024.0f; break;
              case DWT_PLEN_512  : preamblelen = 512.0f; break;
              case DWT_PLEN_256  : preamblelen = 256.0f; break;
              case DWT_PLEN_128  : preamblelen = 128.0f; break;
              case DWT_PLEN_64   : preamblelen = 64.0f; break;
        }

	//preamble  = plen * (994 or 1018) depending on 16 or 64 PRF
	if(instance_data.configData.prf == DWT_PRF_16M)
	{
		preamblelen = (sfdlen + preamblelen) * 0.99359f;
	}
	else
	{
		preamblelen = (sfdlen + preamblelen) * 1.01763f;
	}


	//total time the frame takes in symbols
	return (16 + (int)((preamblelen + (msgdatalen/1000.0))/ 1.0256));

}

// -------------------------------------------------------------------------------------------------------------------
// function to add new anchor in list
//
void inst_add_anchor(uint8* addr, coords_t c, measure_t m)
{
    uint8 i;
    uint8 blank[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    double res_vec[4] = {0.0f, 0.0f, 0.0f, 0.0f};
   
    //add the new Anchor to the list, if it is not already there and if there is a free space
    for(i = 0; i < ANCHOR_LIST_SIZE; i++)
    {
        if(memcmp(&instance_data.Ancs[i].addr[0], &addr[0], ADDR_BYTE_SIZE_L) != 0) //if it is not the same address
        {
            if(memcmp(&instance_data.Ancs[i].addr[0], &blank[0], ADDR_BYTE_SIZE_L) == 0) //if it is a free space
            {
                memcpy(&instance_data.Ancs[i].addr[0], &addr[0], ADDR_BYTE_SIZE_L) ;
                instance_data.Ancs[i].kf = KalmanFilter(&instance_data.Ancs[i].kf);
                break;
            }
        }
        else break;
    }
    if (i == ANCHOR_LIST_SIZE) return;
    
    res_vec[0] = m.range; res_vec[1] = m.vel; res_vec[2] = m.noise; res_vec[3] = m.bias;
    if (!instance_data.Ancs[i].kf.initialized) SetState(res_vec, &instance_data.Ancs[i].kf);
    else Correct(&res_vec[0], &instance_data.Ancs[i].kf);
    instance_data.Ancs[i].coords = c;
    instance_data.Ancs[i].coords.cL = instance_data.Ancs[i].kf.State.Data[0][0];
    instance_data.Ancs[i].measures = m;
    instance_data.anc_hist[instance_data.configData.trxCode]++;
    if (!instance_data.Ancs[i].connected) 
    {
        instance_data.Ancs[i].connected = true;
        instance_data.ai++;
        instance_data.newrange = true;
    }
} // end inst_add_anchor()

// -------------------------------------------------------------------------------------------------------------------
//
// Function to check anchor positions - 
//      if they all stay in line, there in no solution to find self coords
//
uint8 is_line(coords_t c[3])
{
    double k12, k13, b12, b13;
    uint64 s12 = 0, s13 = 0;
    
    if (c[0].cX != c[1].cX)
    {
        k12 = (c[0].cY - c[1].cY) / (c[0].cX - c[1].cX);
        b12 = c[0].cY - (k12 * c[0].cX);
        s12 = *(uint64*)(&k12) & 0x8000000000000000;
    }
    else
    {
        k12 = 100500;
        b12 = c[0].cX;
    }
    
    if (c[0].cX != c[2].cX)
    {
        k13 = (c[0].cY - c[2].cY) / (c[0].cX - c[2].cX);
        b13 = c[0].cY - (k13 * c[0].cX);
        s13 = *(uint64*)(&k13) & 0x8000000000000000;
    }
    else
    {
        k13 = 100500;
        b13 = c[0].cX;
    }
    
    if (s12 == s13 && abs(k12 - k13) < 0.01 && abs(b12 - b13) < 0.01) return 1;
    else return 0;
}

// -------------------------------------------------------------------------------------------------------------------
//
// Standalone function to calculate tag coords
//
coords_t instcalccoords(void)
{
    coords_t res = { 0.NaN, 0.NaN, 0 }; //result
    coords_t c[3];              //coords of connected anchors
    
    for (uint8 i = 0; i < ANCHOR_LIST_SIZE && instance_data.ai > 0; i++) 
    {
        if (instance_data.Ancs[i].connected)
        {
            c[--instance_data.ai] = instance_data.Ancs[i].coords;
            instance_data.Ancs[i].connected = false;
        }
    }
    
    if (!is_line(c))
    {
        /* Magic method - I don't remember why it works*/    
        double k = (c[2].cX - c[0].cX) / (c[1].cX - c[0].cX);
        double a2 = pow(c[1].cY, 2) - pow(c[0].cY, 2) + pow(c[1].cX, 2) - 
                    pow(c[0].cX, 2) + pow(c[0].cL, 2) - pow(c[1].cL, 2);
        double a3 = pow(c[2].cY, 2) - pow(c[0].cY, 2) + pow(c[2].cX, 2) - 
                    pow(c[0].cX, 2) + pow(c[0].cL, 2) - pow(c[2].cL, 2); 

        res.cY = (a3 - (a2 * k)) / (2 * ((c[0].cY - c[1].cY) * k - (c[0].cY - c[2].cY)));
        res.cX = (2 * res.cY * (c[0].cY - c[1].cY) + a2) / (2 * (c[1].cX - c[0].cX));
    }
    
    return res;
}

// select next preamble code
void inst_select_code(void)
{
    if (instance_data.mode == TAG) 
    {
#if defined(STM32F410Cx)
        instance_data.configData.trxCode = get_lrand_range(MAX_PCODE);        //from 0 to 16
#elif defined(STM32F411xE)
        instance_data.configData.trxCode = (MAX_PCODE * mts_drand(&mtwist));  //from 0 to 16
#endif
        dwt_settrxcode(instance_data.configData.trxCode + 9);                 //set new preamble code - from 9 to 24
    }
    instance_data.fwto = 5715;                                                //set beacon timeout
    instance_data.testAppState = TA_RXE_WAIT;
}

// -------------------------------------------------------------------------------------------------------------------
//
// Start the software timeout to 0.5 + [0...1] ms
//
void inst_startswtimer(void)
{
    int delay = *clock_count.ticks + MIN_DELAY;

#if defined(STM32F410Cx)
    delay += get_lrand_range(RAND_DELAY);
#elif defined(STM32F411xE)
    delay += (RAND_DELAY * mts_drand(&mtwist));
#endif
    *event_wdg.ticks = 0;
    *event_wdg.timer = delay;           //(delay < TICKS_IN_UNIT ? delay : delay - TICKS_IN_UNIT) - if we use only one timer
    *event_wdg.timer_en = true;
}

// -------------------------------------------------------------------------------------------------------------------
//
// Main lifecycle, which ends when the new range occurs
//
int instance_run(void)
{
    if (!*event_wdg.timer_en)
    {
        instance_data.done = INST_NOT_DONE_YET;
        led_off(LED_ALL);
        while(instance_data.done == INST_NOT_DONE_YET) testapprun(&instance_data);
        inst_startswtimer();
    }
    return instance_data.done;
}

// -------------------------------------------------------------------------------------------------------------------
//
// the main Tag instance state machine 
//
// -------------------------------------------------------------------------------------------------------------------
//
int testapprun(instance_data_t *inst)
{
    switch (inst->testAppState)
    {
        case TA_TXPOLL_WAIT_SEND :
        {
            inst->poll_msg.seqNum = inst->frame_sn++; 
            memcpy(&(inst->poll_msg.messageData[X]), &inst->self_coords.cX, 8);
            memcpy(&(inst->poll_msg.messageData[Y]), &inst->self_coords.cY, 8);
                        
            inst->testAppState = TA_TX_WAIT_CONF;       // wait confirmation
            inst->previousState = TA_TXPOLL_WAIT_SEND ;
            inst->fwto = inst->fwtoResp_sy;             // set responce timeout
            
            // write the frame data to the device
            dwt_writetxdata(POLL_FRAME_LEN_BYTES, (uint8 *)&inst->poll_msg, 0) ;	
            dwt_writetxfctrl(POLL_FRAME_LEN_BYTES, 0);
            dwt_starttx(DWT_START_TX_IMMEDIATE);
            
            //start tx event watchdog
            inst_startswtimer();
        }
        break;
        case TA_TXRESPONSE_WAIT_SEND :
        {
            //increment seq number
            inst->resp_msg.seqNum = inst->frame_sn++;
            memcpy(&(inst->resp_msg.messageData[X]), &inst->self_coords.cX, 8);
            memcpy(&(inst->resp_msg.messageData[Y]), &inst->self_coords.cY, 8);
            
            inst->testAppState = TA_TX_WAIT_CONF;       // wait confirmation
            inst->previousState = TA_TXRESPONSE_WAIT_SEND ;
            
            //calculate delay for delayed transmission
            inst->Delay = readdevicetime() + inst->txresponseDelay_tu;
            dwt_setdelayedtrxtime((uint32)(inst->Delay >> 8));
            inst->Ancs[inst->ai].ToF.TX_RESP = inst->Delay  - inst->Ancs[inst->ai].ToF.RX_POLL + inst->txantennaDelay;
            
            //write poll-response anchor round time into the message 
            memcpy(&(inst->resp_msg.messageData[PLRS]), &(inst->Ancs[inst->ai].ToF.TX_RESP), 5);
            
            // write the frame data to the device
            dwt_writetxdata(RESP_FRAME_LEN_BYTES, (uint8 *)&inst->resp_msg, 0) ;	
            dwt_writetxfctrl(RESP_FRAME_LEN_BYTES, 0);
            dwt_starttx(DWT_START_TX_DELAYED | DWT_RESPONSE_EXPECTED);
            
            //start tx event watchdog
            inst_startswtimer();
        }
        break;
        case TA_TX_WAIT_CONF :
        {
            instance_getevent(&dw_event); //get and clear this event
            if (dw_event.type != DWT_SIG_TX_DONE) //wait for TX done confirmation
            {
                if (!*event_wdg.timer_en)         //sw timeout had been expired => transmitter error (!!!) 
                {
                    dwt_forcetrxoff();                                  //reset the transmitter
                    inst->testAppState = TA_TXPOLL_WAIT_SEND;           //send another poll
                    inst->done = INST_DONE_WAIT_FOR_NEXT_EVENT_TO;      //instance done failture
                }
                break;
            }
            *event_wdg.timer_en = false;
            if (inst->previousState == TA_TXPOLL_WAIT_SEND) 
            {
                inst->Ancs[inst->ai].ToF.TX_POLL = dw_event.timeStamp;
                inst->txPolls++;
            }
            else inst_select_code();                                          //select next code
        }                                                                     //fall to the next case immidiately
        case TA_RXE_WAIT :
        {
            dwt_setrxtimeout(inst->fwto);                 //units are symbols 
            instancerxon(inst, 0, 0) ;                    //turn RX on without delay

            inst->testAppState = TA_RX_WAIT_DATA;   // let this state handle it
            // end case TA_RXE_WAIT, don't break, but fall through into the TA_RX_WAIT_DATA state to process it immediately.
        }
        case TA_RX_WAIT_DATA :                                               // Wait RX data
        {
            instance_getevent(&dw_event);                                    //get and clear last event
            switch (dw_event.type)
            {
                //if we have received a DWT_SIG_RX_OKAY event - this means that the message is IEEE data type - need to check frame control to know which addressing mode is used
                case DWT_SIG_RX_OKAY :
                {
                    int fn_code = 0;

                    if (dw_event.msgu.frame[frCtrl0] == 0) fn_code = RTLS_DEMO_MSG_POLL;
                    else fn_code = RTLS_DEMO_MSG_RESP;
                                            
                    switch(fn_code)
                    {
                        case RTLS_DEMO_MSG_RESP:
                        {
                            coords_t c;
                            memcpy(&c.cX, &dw_event.msgu.resp.messageData[X], 8);
                            memcpy(&c.cY, &dw_event.msgu.resp.messageData[Y], 8);
                            if (!isnan(c.cX) && !isnan(c.cY) && inst->mode == TAG)
                            {
                                inst->Ancs[inst->ai].ToF.RX_RESP = dw_event.timeStamp - 
                                                                   inst->Ancs[inst->ai].ToF.TX_POLL;   // times measured at Anchor extracted from the message buffer
                                memcpy(&inst->Ancs[inst->ai].ToF.TX_RESP, &(dw_event.msgu.resp.messageData[PLRS]), 5);
                                measure_t m = reportTOF();
                                if (m.range > 0)                                //valid ToF
                                {
                                    inst_add_anchor(&dw_event.msgu.resp.sourceAddr[0], c, m);
                                    if (inst->ai == 3)                          //enough anchors connected
                                    {
                                        c = instcalccoords();                   //calc self coords
                                        if (!isnan(inst->self_coords.cX) && !isnan(inst->self_coords.cY))       //valid coords
                                        {
                                            led_on(LED_1);                          //indicate success
                                            double d = sqrt(pow(inst->self_coords.cX - c.cX, 2) + pow(inst->self_coords.cY - c.cY, 2));
                                            if (d < abs(m.bias)) inst->self_coords = c;
                                        }
                                    }
                                }
                            }
                            inst_select_code();                                         //select next code
                            inst->done = INST_DONE_WAIT_FOR_NEXT_EVENT;                 //instance done    
                        }
                        break;
                        case RTLS_DEMO_MSG_POLL:
                        {
                            if (!isnan(inst->self_coords.cX) && !isnan(inst->self_coords.cY))
                            {
                                memcpy(&inst->resp_msg.destAddr[0], &dw_event.msgu.poll.sourceAddr[0], ADDR_BYTE_SIZE_L);
                                inst->Ancs[inst->ai].ToF.RX_POLL = dw_event.timeStamp;
                                    
                                inst->testAppState = TA_TXRESPONSE_WAIT_SEND;
                            }
                            else inst_select_code();                  //select next code
                            break;
                        }
                    } //end switch (fcode)
                }
		break ; //end of DWT_SIG_RX_OKAY
                case DWT_SIG_RX_TIMEOUT :
                {
                    dwt_setrxtimeout(DISABLE);
                    led_on(LED_2);                                      //indicate error
                    inst->rxTimeouts++;
                    if (inst->mode == TAG)
                    {
                        if (inst->rxTimeouts%2 == 0) 
                        {
                            inst->rxTimeouts = 0;
                            inst_select_code();
                        }
                        else inst->testAppState = TA_TXPOLL_WAIT_SEND; 
                    }
                    else 
                    {
                        dwt_forcetrxoff();      //this will clear all events
                        dwt_rxreset();
                        inst->testAppState = TA_RXE_WAIT;
                    }
                    inst->done = INST_DONE_WAIT_FOR_NEXT_EVENT_TO;           //instance done failture
                }
                break ;
		default :
                  break;
            }
            break ; // end case TA_RX_WAIT_DATA
            default:
                //ERROR
            break;
        }
    } // end switch on testAppState

    return inst->done;
} // end testapprun()

// -------------------------------------------------------------------------------------------------------------------
// function to initialise tag instance structures
//
// Returns 0 on success and -1 on error
int instance_init_s(INST_MODE mode)
{
    // if using auto CRC check (DWT_INT_RFCG and DWT_INT_RFCE) are used instead of DWT_INT_RDFR flag
    dwt_setinterrupt(DWT_INT_TFRS | DWT_INT_RFCG | DWT_INT_RFTO, 1);
    //dwt_setinterrupt(DWT_INT_ALL, 1);
    dwt_setcallbacks(instance_txcallback, instance_rxcallback);

    instance_data.panid = 0xdeca ;                      //set PAN id
    dwt_setpanid(instance_data.panid);
    
    //set eui
    uint64 arand = 0;
#if defined(STM32F410Cx)
    arand = get_llrand();
#elif defined(STM32F411xE)
    uint32 newseed = (uint32)readdevicetime(); //initialize MTwist
    mts_seed32(&mtwist, newseed);
    arand = mts_llrand(&mtwist);         //create new random address
#endif
    memcpy(&instance_data.eui[0], &arand, 8);
    dwt_seteui(instance_data.eui);
    
    instance_data.mode = mode;
    if (mode == TAG) instance_data.self_coords.cX = instance_data.self_coords.cY = 0.NaN; //NaN
    //config radio frames with preset sender and reciever address
    instanceconfigframe((uint8*)&instance_data.poll_msg, RTLS_DEMO_MSG_POLL);
    instanceconfigframe((uint8*)&instance_data.resp_msg, RTLS_DEMO_MSG_RESP);
    uint64 to = TIMEOUT_MULTIPLE * instanceframeduration(RESP_FRAME_LEN_BYTES); //calc timeouts
    if (to > 0xffff) instance_data.fwtoResp_sy = 0xffff;  
    else instance_data.fwtoResp_sy = to;  
    instance_data.txresponseDelay_tu = convertmicrosectodevicetimeu((double)(310.0)) - 50; //magic numbers??? 
 
    dwt_setrxtimeout(DISABLE);
    dwt_setrxaftertxdelay(DISABLE);
    dwt_enableframefilter(DWT_FF_COORD_EN | DWT_FF_BEACON_EN | DWT_FF_DATA_EN); //enable beacon and data frame type
    dwt_setautorxreenable(true);                               //not necessary to auto RX re-enable as the receiver is on for a short time (Tag knows when the response is coming)
    dwt_setdblrxbuffmode(DOUBLE_RX_BUFFER);                    //disable double RX buffer
    
    inst_select_code();
    
    return 0 ;
}
// -------------------------------------------------------------------------------------------------------------------
// function to set the fixed reply delay time (in ms)
//


/* ==========================================================

Notes:

Previously code handled multiple instances in a single console application

Now have changed it to do a single instance only. With minimal code changes...(i.e. kept [instance] index but it is always 0.

Windows application should call instance_init() once and then in the "main loop" call instance_run().

*/
