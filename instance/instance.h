/*! ----------------------------------------------------------------------------
 *  @file    instance.h
 *  @brief   DecaWave header for application level instance
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */
#ifndef _INSTANCE_H_
#define _INSTANCE_H_

#include "kf.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "deca_device_api.h"
/******************************************************************************************************************
********************* NOTES on DW (MP) features/options ***********************************************************
*******************************************************************************************************************/
//Note:	Double buffer mode can only be used with auto rx re-enable, auto rx re-enable is on by default in Listener and Anchor instances
#define DOUBLE_RX_BUFFER (0) //To enable double RX buffer set this to 1 - this only works for the Listener instance
//NOTE: this feature is really meant for a RX only instance, as TX will not be possible while double-buffer and auto rx-enable is on.
#define CORRECT_RANGE_BIAS  (1)     // Compensate for small bias due to uneven accumulator growth at close up high power

/******************************************************************************************************************
*******************************************************************************************************************
*******************************************************************************************************************/
#define DWT_PRF_64M_RFDLY   (514.462f)
#define DWT_PRF_16M_RFDLY   (513.9067f)

#define SPEED_OF_LIGHT      (299702547.0)     // in m/s in air

//Fast 2WR function codes
#define RTLS_DEMO_MSG_POLL                 (0x50)          // Tag poll message
#define RTLS_DEMO_MSG_RESP                 (0x52)          // Anchor response to poll
//lengths including the Decaranging Message Function Code byte
#define RTLS_POLL_MSG_LEN                   17		    // FunctionCode(1), TAGx(8), TAGy(8)
#define RTLS_RESP_MSG_LEN                   22              // FunctionCode(1), ANCx(8), ANCy(8), PollResp_time(5)
// Total frame lengths.
#define POLL_FRAME_LEN_BYTES                (RTLS_POLL_MSG_LEN + FRAME_CRTL_AND_ADDRESS_LN + FRAME_CRC)
#define RESP_FRAME_LEN_BYTES                (RTLS_RESP_MSG_LEN + FRAME_CRTL_AND_ADDRESS_L + FRAME_CRC)

#define STANDARD_FRAME_SIZE         127
#define ADDR_BYTE_SIZE_L            (8)
#define FRAME_CONTROL_BYTES         2
#define FRAME_SEQ_NUM_BYTES         1
#define FRAME_PANID                 2
#define FRAME_CRC		    2
#define FRAME_CTRLP		        (FRAME_CONTROL_BYTES + FRAME_SEQ_NUM_BYTES + FRAME_PANID) //5
#define FRAME_CRTL_AND_ADDRESS_LN       (ADDR_BYTE_SIZE_L + FRAME_CTRLP) //13
#define FRAME_CRTL_AND_ADDRESS_L        (ADDR_BYTE_SIZE_L + ADDR_BYTE_SIZE_L + FRAME_CTRLP) //21 bytes for 64-bit addresses)
#define MAX_USER_PAYLOAD_STRING_LN      (STANDARD_FRAME_SIZE-FRAME_CRTL_AND_ADDRESS_LN-FRAME_CRC) //127 - 13 - 2 = 112
#define MAX_USER_PAYLOAD_STRING_LL      (STANDARD_FRAME_SIZE-FRAME_CRTL_AND_ADDRESS_L-FRAME_CRC) //127 - 21 - 2 = 104

//response delay time (Tag or Anchor when sending Response messages respectively)
#define TIMEOUT_MULTIPLE                (2.5)
#define MAX_NUMBER_OF_POLL_RETRYS       (5)
#define MAX_EVENT_NUMBER                (10)
#define ANCHOR_LIST_SIZE                (20)

#define MIN_DELAY                       (400)
#define RAND_DELAY                      (1100)

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// NOTE: the maximum RX timeout is ~ 65ms
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define INST_DONE_NO_EVENT                  3   //this signifies that no event has come
#define INST_DONE_WAIT_FOR_NEXT_EVENT       1   //this signifies that the current event has been processed and instance is ready for next one
#define INST_DONE_WAIT_FOR_NEXT_EVENT_TO    2   //this signifies that the current event has been processed and that instance is waiting for next one with a timeout
                                                //which will trigger if no event coming in specified time
#define INST_NOT_DONE_YET                   0   //this signifies that the instance is still processing the current event

#define MAX_PCODE                           16
typedef enum instanceModes{LISTNER, TAG, ANCHOR} INST_MODE;

//Listener = in this mode, the instance only receives frames, does not respond
//Tag = Exchanges DecaRanging messages (Poll-Response-Final) with Anchor and enabling Anchor to calculate the range between the two instances
//Anchor = see above

typedef enum inst_states
{
    TA_TX_WAIT_CONF = 1,        //1
    TA_TXPOLL_WAIT_SEND,        //2
    TA_TXRESPONSE_WAIT_SEND,    //3

    TA_RXE_WAIT,                //4
    TA_RX_WAIT_DATA,            //5
} INST_STATES;


// This file defines data and functions for access to Parameters in the Device
#pragma pack(push, 1)
//structure for Poll message
typedef struct
{
    uint8 frameCtrl[2];                         	//  frame control bytes 00-01
    uint8 seqNum;                               	//  device hop level 02
    uint8 panID[2];                             	//  PAN ID 03-04
    uint8 sourceAddr[ADDR_BYTE_SIZE_L];           	//  05-14 using 64 bit addresses
    uint8 messageData[MAX_USER_PAYLOAD_STRING_LN] ;        //  15-124 (application data and any user payload)
    uint8 fcs[2] ;                              	//  125-126  we allow space for the CRC as it is logically part of the message. However ScenSor TX calculates and adds these bytes.
} srd_msg_dnsl ;

//structure for Resp message 
typedef struct
{
    uint8 frameCtrl[2];                         	//  frame control bytes 00-01
    uint8 seqNum;                               	//  device hop level 02
    uint8 panID[2];                             	//  PAN ID 03-04
    uint8 destAddr[ADDR_BYTE_SIZE_L];             	//  05-12 using 64 bit addresses
    uint8 sourceAddr[ADDR_BYTE_SIZE_L];           	//  13-20 using 64 bit addresses
    uint8 messageData[MAX_USER_PAYLOAD_STRING_LL] ;     //  22-124 (application data and any user payload)
    uint8 fcs[2] ;                              	//  125-126  we allow space for the CRC as it is logically part of the message. However ScenSor TX calculates and adds these bytes.
} srd_msg_dlsl ;

typedef struct
{
    uint8 channelNumber ;       // valid range is 1 to 11
    uint8 pulseRepFreq ;        // NOMINAL_4M, NOMINAL_16M, or NOMINAL_64M
    uint8 dataRate ;            // DATA_RATE_1 (110K), DATA_RATE_2 (850K), DATA_RATE_3 (6M81)
    uint8 preambleCode ;        // 00 = use NS code, 1 to 24 selects code
    uint8 preambleLen ;         // values expected are 64, (128), (256), (512), 1024, (2048), and 4096
    uint8 pacSize ;
    uint8 nsSFD ;
    uint16 sfdTO;               //!< SFD timeout value (in symbols) e.g. preamble length (128) + SFD(8) - PAC + some margin ~ 135us... DWT_SFDTOC_DEF; //default value
    uint8 phrMode;
    uint8 powerTX;              //for manual use only
} instanceConfig_t ;

typedef struct
{
	uint32 icid;
	dwt_rxdiag_t diag;
} devicelogdata_t ;

/******************************************************************************************************************
*******************************************************************************************************************
*******************************************************************************************************************/

//NOTE: Accumulators don't need to be stored as part of the event structure as when reading them only one RX event can happen...
//the receiver is singly buffered and will stop after a frame is received

typedef struct
{
 	uint8  type;			// event type
	uint16 rxLength ;

	uint64 timeStamp ;		   // last timestamp (Tx or Rx)
	uint32 timeStamp32l ;		   // last tx/rx timestamp - low 32 bits
	uint32 timeStamp32h ;		   // last tx/rx timestamp - high 32 bits

	union 
        {
            //holds received frame (after a good RX frame event)
            uint8   frame[STANDARD_FRAME_SIZE];
            srd_msg_dnsl poll ; //only src address
            srd_msg_dlsl resp ; //dst and src addresses
            
	}msgu;

} event_data_t ;

typedef struct {
                uint8 PGdelay;

                //TX POWER
                //31:24     BOOST_0.125ms_PWR
                //23:16     BOOST_0.25ms_PWR-TX_SHR_PWR
                //15:8      BOOST_0.5ms_PWR-TX_PHR_PWR
                //7:0       DEFAULT_PWR-TX_DATA_PWR
                uint32 txPwr[2]; //
} tx_struct;
#pragma pack(pop)

typedef struct
{
    uint64 TX_POLL;
    uint64 RX_POLL;
    uint64 TX_RESP;
    uint64 RX_RESP;             
} timestamp_t;

typedef struct
{
    double cX;
    double cY;
    double cL;
} coords_t;

typedef struct
{
    double range;
    double vel;
    double bias;
    double noise;
} measure_t;

typedef struct 
{
    uint8       addr[ADDR_BYTE_SIZE_L]; // 64 bit addresses
    timestamp_t ToF;                    // DWT timestamp values of rx and tx messages
    uint64      dt;                     // time between two last ranges
    coords_t    coords;                 // coordinates - X, Y
    measure_t   measures;               // measurements - range, velocity, bias, noise
    filter_t    kf;                     // Kalman filter
    bool        connected;              // connection flag
} anchor_data_t;

typedef struct
{
    INST_MODE mode;			//instance mode (tag or anchor)

    INST_STATES testAppState ;		//state machine - current state
    INST_STATES previousState ;		//state machine - previous state
    int done ;				//done with the current event/wait for next event to arrive
    
    //configuration structures
    dwt_config_t    configData ;	//DW1000 channel configuration
    dwt_txconfig_t  configTX ;		//DW1000 TX power configuration
    dwt_rxdiag_t    diagRX ;            //recieved signal noise and level
    uint16	    txantennaDelay ;    //DW1000 TX antenna delay
    
    //message structures used for transmitted messages
    // simple 802.15.4 frame structure (used for tx message) - using long addresses
    srd_msg_dnsl poll_msg ;	// poll message (destination long, source none)
    srd_msg_dlsl resp_msg ;	// resp message (destination long, source long)
    uint16 fwtoResp_sy;         // response message timeout
    uint16 fwto;                // waiting message timeout
    uint64 txresponseDelay_tu;  // frame transmission / reception delay time in device time units

    //Tag function address/message configuration
    uint8   eui[ADDR_BYTE_SIZE_L];              // devices EUI 64-bit address
    uint8   frame_sn;				// modulo 256 frame sequence number - it is incremented for each new frame transmittion
    uint16  panid ;			        // panid used in the frames
    
    //diagnostic counters/data, results and logging
    coords_t self_coords;                       //calculated tag coordinates
    anchor_data_t Ancs[ANCHOR_LIST_SIZE];       //list of anchors 
    uint8 ai;                                   //count of connected anchors
    
    uint64 Delay;               //timestamp that DW1000 should wait before start tx or rx
    bool newrange;              //flag of new range
    
    //this three variables used only to collect statistic
    uint32 rxTimeouts;                          //timeouts counter
    uint32 txPolls;                             //total amount of sended polls
    uint16 anc_hist[ANCHOR_LIST_SIZE];          //trx code histogramm

    //event queue - used to store DW1000 events as they are processed by the dw_isr/callback functions
    event_data_t dwevent[MAX_EVENT_NUMBER]; //this holds any TX/RX events and associated message data
    volatile uint8 lastEventInd;
} instance_data_t ;
//-------------------------------------------------------------------------------------------------------------
//
//	Functions used in driving/controlling the ranging application
//
//-------------------------------------------------------------------------------------------------------------
// calls the DW1000 interrupt handler
#define instance_process_dwt_irq(x) 	dwt_isr()  //call device interrupt handler

//------------------------------Common functions---------------------------------------------------------------
// functions to convert DW1000 time to seconds and vice versa
uint32 convertmicrosectodevicetimeu32 (double microsecu);
uint64 convertmicrosectodevicetimeu (double microsecu);
double convertdevicetimetosec(int32 dt);
double convertdevicetimetosec8(uint8* dt);

// function to calculate and report the Time of Flight to the GUI/display
bool instancenewrange(void);
// fill anchor list 
void instance_fillanclist(void) ;

// configure the instance and DW1000 device
// uint8 manualpoweren - Manual Power enable / disable - the value of tx power is read from otp / set manually
void instance_config(instanceConfig_t *config, uint8 manualpoweren);

// configure TX/RX callback functions that are called from DW1000 ISR
void instance_rxcallback(const dwt_callback_data_t *rxd);
void instance_txcallback(const dwt_callback_data_t *txd);

// functions of event queue routine
void instance_putevent(event_data_t* newevent);
void instance_getevent(event_data_t* e);
void instance_clearevents(void);

// function to construct the message/frame bytes
void instanceconfigframe(uint8 *msg, uint8 fcode);

// function to get the DW1000 internal time
uint64 readdevicetime(void);

// turn on the reciever
void instancerxon(instance_data_t *inst, int delayed, uint64 delayedReceiveTime);

// Calculate the frame duration and waiting timeout in symbols 
uint32 instanceframeduration(int datalength);

//main application lifecycle
int instance_run(void);

//-------------------------------Instance dependent functions-------------------------------------------------
// Call init, then call config, then call run. call close when finished
// initialise the instance (application) structures and DW1000 device
int instance_init_s(INST_MODE mode);
void instance_set_role(INST_MODE mode);
int testapprun(instance_data_t *inst);

extern int state;
extern const uint16 rfDelays[2];
extern const tx_struct txSpectrumConfig[8];
extern uint8 dwnsSFDlen[];
extern instance_data_t instance_data;
#ifdef __cplusplus
}
#endif

#endif
