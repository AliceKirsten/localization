/*! ----------------------------------------------------------------------------
 *  @file    main.c
 *  @brief   main loop for the DecaRanging application
 *
 * @attention
 *
 * Copyright 2015 (c) HTSYS, Moscow, Russia.
 *
 * All rights reserved.
 *
 * @author HTSYS
 */
/* Includes */
#include "port.h"
#include "instance.h"
#pragma message ("This is a multipiont localization application.")

#if (FLASH_ENABLE == 1)
    uint16 flash_addr = sFLASH_START_ADDR; 
    double ranges[100];
    uint64_t times[100];
#endif

const coords_t anc_def_coords[3] = {{-0.6, 0.0, 0}, {-1.5, 5.0, 0}, {2.0, 2.5, 0}};
INST_MODE instance_mode = TAG;
//Configuration for DecaRanging Mode
instanceConfig_t chConfig =
//RC_MODE == 4
{
    4,              // channel
    DWT_PRF_64M,    // prf
    DWT_BR_850K,    // datarate
    9,              // preambleCode
    DWT_PLEN_256,   // preambleLength
    DWT_PAC16,      // pacSize
    0,              // non-standard SFD
    (257 + 32 - 16),//SFD timeout
    DWT_PHRMODE_STD //PHR mode
};

int inittestapplication(void)
{
    uint32 devID = 0;
    int result;

    reset_DW1000();             //reset the DW1000 by driving the RSTn line low
    //we can enable any configuration loding from OTP/ROM on initialisation
    result = dwt_initialise(DWT_LOADUCODE | DWT_LOADLDO | DWT_LOADTXCONFIG | DWT_LOADANTDLY| DWT_LOADXTALTRIM) ;
    if (DEV_SUCCESS != result) return (-1) ;   // device initialise has failed
    
    //this is platform dependant - only program if DW EVK/EVB
    dwt_setleds(3) ; //configure the GPIOs which control the leds on EVBs

    SPI_ChangeRate(DW1000_SPI, SPI_BaudRatePrescaler_4); //increase SPI to max
    
    devID = dwt_readdevid();
    if (DWT_DEVICE_ID != devID) return(-1) ; // SPI not working or Unsupported Device ID
    
    instance_config(&chConfig, 0);           // Set operating channel etc
    instance_init_s(instance_mode);          // Set common instance parameters

    return 0;
}
/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/
#if (FLASH_ENABLE == 1)
void sFLASH_WritePacket(unsigned long time, uint8 range)
{
   uint8 rbuf[6] = {(uint8)(sFLASH_DUMMY_BYTE|button_status),
                    (uint8)((time & 0xff000000) >> 24), (uint8)((time & 0x00ff0000) >> 16), 
                    (uint8)((time & 0x0000ff00) >> 8), (uint8)(time & 0x000000ff),  
                    (uint8)(range)};
   sFLASH_WriteBuffer(&rbuf[0], flash_addr, 6);
   flash_addr += 6; 
}
#endif

void process_deca_irq(void)
{
    do
    {
        instance_process_dwt_irq(0);

    }while(port_CheckEXT_IRQ() == 1); //while IRS line active (ARM can only do edge sensitive interrupts)
}

/*
 * @fn      main()
 * @brief   main entry point
**/
int main(void)
{
    //can't initialize clocks - hard fault
    if (peripherals_init() == DEV_ERROR) while(1); 
    
    if (instance_mode == ANCHOR) 
    {
        chConfig.preambleCode = 11;
        memcpy(&instance_data.self_coords, &anc_def_coords[chConfig.preambleCode - 9], sizeof(coords_t));
    }
    
    //DW1000 initialization
    while (inittestapplication() == DEV_ERROR);
    
    //enable ScenSor IRQ before starting
    port_EnableEXT_IRQ();       
    // main loop
    while(1)
    {
        instance_run();         //up to 1.37 ms until first range
    }

    return 0;
}



