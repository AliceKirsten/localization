/******************************************************************************
@brief:
      This is a small library, that gives an ability to operate with matrixes and vectors.
@warning:      
      Warning: the function, that inverse the matrix, can be applied only on a square matrix of 2x2 elements.
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/
#include "compiler.h"

#define USING_DYNAMIC_MEMALLOC                  (0)

//define the type of a matrix or a vector
typedef struct
{
#if (USING_DYNAMIC_MEMALLOC)
     double** Data;
#else
     double Data[2][2];   //data container of Rows x Cols elements
#endif
     int Rows;            //amount of rows
     int Cols;            //amount of columns
} matrix_t;

matrix_t Matrix(double* data, int r, int c, matrix_t* res);     //constructor
#if (USING_DYNAMIC_MEMALLOC)
void deMatrix(matrix_t* m);                                     //destructor, frees the container
#endif

#ifdef __cplusplus
extern "C"
{
    matrix_t operator +(matrix_t m1, matrix_t m2);                  //operator of addiction
    matrix_t operator -(matrix_t m1, matrix_t m2);                  //operator of subtraction
    matrix_t operator *(matrix_t m1, matrix_t m2);                  //operator of multiplication
}
#endif
matrix_t Transp(matrix_t m);                                    //transpose a matrix or a vector
double Det(matrix_t m);                                         //determinant of a square matrix
matrix_t Inv(matrix_t m);                                       //inverse a square matrix 2x2

