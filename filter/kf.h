/******************************************************************************
@brief:
      This is a full library, that cat be used for constructing a standart two-dimensional Kalman Filter
@warning:      
      Warning: 
        1. Use only in conjunction with the library file "matrix.h"
        2. Use only for two-dimensional Kalman Filter
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/

#include "matrix.h"

typedef struct 
{
    bool initialized;    // initialization flag
    
    matrix_t z;          // measurement vector (input)
    
    matrix_t x;          // predicted state vector 
    matrix_t P;          // predicted covariance x matrix
   
    matrix_t F;          // update matrix
    matrix_t H;          // extraction matrix
    matrix_t R;          // covariance of environment noise
    matrix_t Q;          // covariance of measurement noise
   
    matrix_t State;      // updated state (output)
    matrix_t Covariance; // updated covariance matrix
} filter_t;

filter_t KalmanFilter(filter_t* res);                           //constructor
#if (USING_DYNAMIC_MEMALLOC)
void deKalmanFilter(filter_t* f);                               //destructor
#endif
void SetState(double* state,  filter_t* f);                     //initialization
void Correct(double* data,  filter_t* f);                       //correction 