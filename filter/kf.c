/******************************************************************************
@brief:
      This is a source file, that contants the implementation of  Kalman Filter library functions
@warning:      
      Warning: 
        1. Use only in conjunction with the library file "matrix.h"
        2. Use only for two-dimensional Kalman Filter
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/

#include "kf.h"

/* 
brief:
    constructor
args:
    double* r - a pointer to a one-dimension array of double, 
                that characterize the noise or error of measurement
                of controlled object
    double* r - a pointer to a one-dimension array of double, 
                that characterize the noise or obstacles in measurement
                of the environment
    filter_t* res - a pointer to the result filter structure
*/
filter_t KalmanFilter(filter_t* res)
{
    double init[4] = { 1.0, 1.0, 1.0, 1.0 };       //data array for initialization
    
    res->initialized = false;
    //initialize predicted state vector
    res->x = Matrix(NULL, 2, 1, &res->x);       //2x1
    res->z = Matrix(NULL, 2, 1, &res->z);       //2x1
    //initialize matrixes
    res->F = Matrix(init, 2, 2, &res->F);       //2x2
    res->H = Matrix(init, 2, 2, &res->H);       //2x2
    res->P = Matrix(init, 2, 2, &res->P);       //2x2
    res->R = Matrix(init, 2, 2, &res->R);       //2x2
    res->Q = Matrix(init, 2, 2, &res->Q);       //2x2
    res->Covariance = Matrix(NULL, 2, 2, &res->Covariance);     //2x2
    
    return *res;
}

/* 
brief:
    destructor
args:
    filter_t* f - a pointer to the filter structure, that has to be destructed
*/
#if (USING_DYNAMIC_MEMALLOC)
void deKalmanFilter(filter_t* f)
{
    deMatrix(&f->x);
    
    deMatrix(&f->F);
    deMatrix(&f->H);
    deMatrix(&f->P);
    deMatrix(&f->R);
    deMatrix(&f->Q);
    
    deMatrix(&f->State);
    deMatrix(&f->Covariance);
}
#endif
/* 
brief:
    initialization of the state vector
args:
    double* state - a pointer to the array of control object state variables
    filter_t* f - a pointer to the filter structure
*/
void SetState(double* state, filter_t* f)
{
    f->initialized = true;
    f->State = Matrix(state, 2, 1, &f->State);

#if (USING_DYNAMIC_MEMALLOC)
    memcpy(f->Covariance.Data[0][0], f->P.Data[0][0], (f->P.Cols * f->P.Raws * sizeof(double)));
#else
    f->Covariance.Data[0][0] = f->P.Data[0][0] = f->R.Data[0][0] = state[2];    //enviroment noise
    f->Covariance.Data[1][1] = f->P.Data[1][1] = f->R.Data[1][1] = state[2];
    f->Covariance.Data[0][1] = f->P.Data[0][1] = f->R.Data[0][1] = 
    f->Covariance.Data[1][0] = f->P.Data[1][0] = f->R.Data[1][0] = state[2] * 0.01;
    
    f->Q.Data[0][0] = f->Q.Data[1][1] = abs(state[3]);                          //instrumental accuracy noise
    f->Q.Data[0][1] = f->Q.Data[1][0] = abs(state[3]) * 0.01;   
#endif
}

/* 
brief:
    correction of the state vector
args:
    double* data - a pointer to the array of measured values of object's state variables
    filter_t* f - a pointer to the filter structure
*/
void Correct(double* data,  filter_t* f)
{
    //temp containers
    matrix_t S = Matrix(NULL, 2, 2, &S);
    matrix_t K = Matrix(NULL, 2, 2, &K);
  
    //save measured data
    f->z.Data[0][0] = data[0];          //range
    f->z.Data[1][0] = data[1];          //velosity

    //predict x
    f->x = f->F * f->State;
    //predict p
    f->P = ((f->F * f->Covariance) * Transp(f->F)) + f->Q;
    //update s - the covariance of the move, adding up the covariance in move space of the position and the covariance of the measurement
    S = ((f->H * f->P) * Transp(f->H)) + f->R;
    //calculate K - work of the filter
    K = (f->P * Transp(f->H)) * Inv(S);
    //update state
    f->State = f->x + (K * (f->z - (f->H * f->x)));
    f->Covariance = f->P - ((K * f->H) * f->P);
    
#if (USING_DYNAMIC_MEMALLOC)
    deMatrix(&S);
    deMatrix(&K);
#endif
}