/******************************************************************************
@brief:
      This is a source file, that contants the implementation of matrix library functions
@warning:      
      Warning: the function, that inverse the matrix, can be applied only on a square matrix of 2x2 elements.
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/

#include "matrix.h"

/* 
brief:
    constructor
args:
    double* data - a pointer to a one-dimension array of double
    int r - amount of raws in new matrix or vector
    int c - amount of columns in new matrix or vector
    matrix_t* res - a pointer to the result matrix or vector
*/
matrix_t Matrix(double* data, int r, int c, matrix_t* res)
{
    res->Rows = r;
    res->Cols = c;
    res->Data = (double**)malloc(r * sizeof(double*));
    for (int i = 0; i < r; i++)
    {
        res->Data[i] = (double*)malloc(c * sizeof(double));
        if (data != NULL)
        {
            for (int j = 0; j < c; j++)
            {
                res->Data[i][j] = data[(i * c) + j];
            }
        }
    }
    return *res;
}

/* 
brief:
    destructor
args:
    matrix_t* m - a pointer to the matrix or vector, that has to be destructed
*/
void deMatrix(matrix_t* m)
{
    for (int i = 0; i < m->Rows; i++)
    {
        free(m->Data[i]); //free columns
    }
    free(m->Data);  //free raws
}

/* 
brief:
    addition 
args:
    matrix_t m1 - left summand (matrix or vector)
    matrix_t m2 - right summand (matrix or vector)
*/
matrix_t operator +(matrix_t m1, matrix_t m2)
{
    matrix_t res = Matrix(NULL, m1.Rows, m1.Cols, &res);
    
    for (int i = 0; i < m1.Rows; i++)
    {
        for (int j = 0; j < m1.Cols; j++)
        {
            res.Data[i][j] = m1.Data[i][j] + m2.Data[(i < m2.Rows ? i : 0)][(j < m2.Cols ? j : 0)];
        }
    }
    return res;
}

/* 
brief:
    subtraction 
args:
    matrix_t m1 - minuend (matrix or vector)
    matrix_t m2 - subtrahend (matrix or vector)
*/
matrix_t operator -(matrix_t m1, matrix_t m2)
{
    matrix_t res = Matrix(NULL, m1.Rows, m1.Cols, &res);
    
    for (int i = 0; i < m1.Rows; i++)
    {
        for (int j = 0; j < m1.Cols; j++)
        {
            res.Data[i][j] = m1.Data[i][j] - m2.Data[(i < m2.Rows ? i : 0)][(j < m2.Cols ? j : 0)];
        }
    }
    return res;
}

/* 
brief:
    multiplication 
args:
    matrix_t m1 - left multiplier (matrix or vector)
    matrix_t m2 - right multiplier (matrix or vector)
*/
matrix_t operator *(matrix_t m1, matrix_t m2)
{
     matrix_t res = Matrix(NULL, m1.Rows, m2.Cols, &res);
     
     for (int m = 0; m < m1.Rows; m++)
     {
        for (int n = 0; n < m2.Cols; n++)
        {
            for (int i = 0; i < (m1.Cols < m2.Rows ? m1.Cols : m2.Rows); i++)
            {
                res.Data[m][n] += m1.Data[m][i] * m2.Data[i][n];
            }
        }
     }
     
     return res;
}

/* 
brief:
    transpose
args:
    matrix_t* m - a matrix or vector, that has to be transposed
*/
matrix_t Transp(matrix_t m)
{
    matrix_t res = Matrix(NULL, m.Rows, m.Cols, &res);
    
    for (int i = 0; i < m.Rows; i++)
    {
        for (int j = 0; j < m.Cols; j++)
        {
            res.Data[j][i] = m.Data[i][j];
        }
    }
    
    return res;
}

/* 
brief:
    transpose
args:
    matrix_t m - a matrix or vector, that has to be inversed
*/
matrix_t Inv(matrix_t m)
{
    if (m.Rows == 1 && m.Cols == 1) return m; //if it is a 1x1 dimension matrix
    
    matrix_t res = Matrix(NULL, m.Rows, m.Cols, &res);

    double det = (m.Data[0][0] * m.Data[1][1]) - (m.Data[0][1] * m.Data[1][0]); //calc determinant
    res.Data[0][0] = m.Data[1][1] / det;
    res.Data[0][1] = -m.Data[0][1] / det;
    res.Data[1][0] = -m.Data[1][0] / det;
    res.Data[1][1] = m.Data[0][0] / det;

    return res;
}