/******************************************************************************
@brief:
      This is a full library, that cat be used for constructing a standart two-dimensional Kalman Filter
@warning:      
      Warning: 
        1. Use only in conjunction with the library file "matrix.h"
        2. Use only for two-dimensional Kalman Filter
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/

#include "compiler.h"
#include "matrix.h"

typedef struct 
{
   matrix_t x;          //predicted state vector 
   matrix_t z;          //measurement vector (input)
   
   matrix_t F;          // update matrix
   matrix_t H;          // extraction matrix
   matrix_t P;          // predicted covariance x matrix
   matrix_t R;          // covariance z matrix
   matrix_t Q;          // noise
   
   matrix_t State;      //updated state (output)
   matrix_t Covariance; //state covariance matrix
} filter_t;

filter_t KalmanFilter(double* r, double* q, filter_t* res);     //constructor
void deKalmanFilter(filter_t* f);                               //destructor
void SetState(double* state,  filter_t* f);                     //initialization
void Correct(double* data,  filter_t* f);                       //correction 