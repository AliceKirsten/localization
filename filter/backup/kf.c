/******************************************************************************
@brief:
      This is a source file, that contants the implementation of  Kalman Filter library functions
@warning:      
      Warning: 
        1. Use only in conjunction with the library file "matrix.h"
        2. Use only for two-dimensional Kalman Filter
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/

#include "kf.h"

/* 
brief:
    constructor
args:
    double* r - a pointer to a one-dimension array of double, 
                that characterize the noise or error of measurement
                of controlled object
    double* r - a pointer to a one-dimension array of double, 
                that characterize the noise or obstacles in measurement
                of the environment
    filter_t* res - a pointer to the result filter structure
*/
filter_t KalmanFilter(double* r, double* q, filter_t* res)
{
    double f[4] = { 1.0, 1.0, 0.0, 1.0 };       //data array for update matrix
    double h[4] = { 1.0, 0.0, 0.0, 1.0 };       //data array for extraction matrix
    double p[4] = { 5.0, 0.0, 0.0, 0.5 };       //data array for predicted covariance x matrix
    
    //initialize predicted state vector
    res->x = Matrix(NULL, 2, 1, &res->x);
    //initialize matrixes
    res->F = Matrix(f, 2, 2, &res->F);
    res->H = Matrix(h, 2, 2, &res->H); 
    res->P = Matrix(p, 2, 2, &res->P);
    res->R = Matrix(r, 2, 2, &res->R);
    res->Q = Matrix(q, 2, 2, &res->Q);
    res->Covariance = Matrix(NULL, 2, 2, &res->Covariance);
    
    return *res;
}

/* 
brief:
    destructor
args:
    filter_t* f - a pointer to the filter structure, that has to be destructed
*/
void deKalmanFilter(filter_t* f)
{
    deMatrix(&f->x);
    
    deMatrix(&f->F);
    deMatrix(&f->H);
    deMatrix(&f->P);
    deMatrix(&f->R);
    deMatrix(&f->Q);
    
    deMatrix(&f->State);
    deMatrix(&f->Covariance);
}

/* 
brief:
    initialization of the state vector
args:
    double* state - a pointer to the array of control object state variables
    filter_t* f - a pointer to the filter structure
*/
void SetState(double* state, filter_t* f)
{
    f->State = Matrix(state, 2, 1, &f->State);
    memcpy(f->Covariance.Data[0], f->P.Data[0], f->P.Cols * f->P.Rows);
}

/* 
brief:
    correction of the state vector
args:
    double* data - a pointer to the array of measured values of object's state variables
    filter_t* f - a pointer to the filter structure
*/
void Correct(double* data,  filter_t* f)
{
    //temp containers
    matrix_t S = Matrix(NULL, 2, 2, &S);
    matrix_t K = Matrix(NULL, 2, 2, &K);
  
    //save measured data
    f->z = Matrix(data, 2, 1, &f->z);
    //predict x
    f->x = f->F * f->State;
    //update p
    f->P = ((f->F * f->Covariance) * Transp(f->F)) + f->Q;
    //update s - the covariance of the move, adding up the covariance in move space of the position and the covariance of the measurement
    S = ((f->H * f->P) * Transp(f->H)) + f->R;
    //calculate K - work of the filter
    K = (f->P * Transp(f->H)) * Inv(S);
    //update state
    f->State = f->x + (K * (f->z - (f->H * f->x)));
    f->Covariance = f->P - ((K * f->H) * f->P);
    
    deMatrix(&S);
    deMatrix(&K);
    deMatrix(&f->z);
}