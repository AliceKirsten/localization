/******************************************************************************
@brief:
      This is a small library, that gives an ability to operate with matrixes and vectors.
@warning:      
      Warning: the function, that inverse the matrix, can be applied only on a square matrix of 2x2 elements.
@disclaimer:
      (c) htsys, Moscow, Russia
*******************************************************************************/
#include "compiler.h"

//define the type of a matrix or a vector
typedef struct
{
     double** Data;       //data container of Rows x Cols elements
     int Rows;            //amount of rows
     int Cols;            //amount of columns
} matrix_t;

matrix_t Matrix(double* data, int r, int c, matrix_t* res);     //constructor
void deMatrix(matrix_t* m);                                     //destructor, frees the container
matrix_t operator +(matrix_t m1, matrix_t m2);                  //operator of addiction
matrix_t operator -(matrix_t m1, matrix_t m2);                  //operator of subtraction
matrix_t operator *(matrix_t m1, matrix_t m2);                  //operator of multiplication
matrix_t Transp(matrix_t m);                                    //transpose a matrix or a vector
matrix_t Inv(matrix_t m);                                       //inverse a square matrix of 2x2 elements

